@extends('layouts.master')

@section('content')
<div class="ideanak-content new-style">
	 <div class="top-banner-section">
    <div class="banner-img">
      <img class="products-img kotaksusu" src="{{ asset('assets/semangatibu/images/mobile-kotaksusu.png') }}">
      <img class="mobile-ideanak visible-xs-block" src="{{ asset('assets/semangatibu/images/mobile-ideanak.png') }}">
      <img class="ideanak-banner hidden-xs" src="{{ asset('assets/semangatibu/images/ideanak-banner.png') }}">
    </div>
    <div class="top-banner-copy">
      <div class="opening-text">
        <h1>
         Ups, Halaman tidak ditemukan
        </h1>
      </div>
   
    </div>
  </div>

</div>
@stop