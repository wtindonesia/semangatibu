<nav class="navbar navbar-fixed-top ide-anak-wow-nav">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar"
          aria-expanded="false">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-logo" href="{{ route('home') }}">
          <img src="{{ asset('assets/semangatibu/images/frisian-flag-logo.png') }}">
        </a>
        <img class="navbar-bottom-bg" src="{{ asset('assets/semangatibu/images/navbar-bottom-bg.png') }}">
      </div>
      <div class="navbar-collapse collapse" id="myNavbar" aria-expanded="false" style="height: 1px;">
        <ul class="nav navbar-nav navbar-right navbar-list">
          <li><a href="{{ route('user.gallery') }}" class="gallery-competition">GALERI</a></li>
          <li>
            @if($loggedUser == false)
              <a href="#" class="open-login-modal info-pendaftaran">IKUTI KOMPETISI</a>
              <div class="login-modal login-modal--mobile">
              <a href="{{ route('user.facebook.login')}}" class="button facebook Login-facebook">
                <span>
                  <i class="fa fa-facebook"></i>
                </span>
                <p>Login with Facebook</p>
              </a>
              <form id="login-form-mobile" method="post" role="form" action="{{ route('login.submit.ajax') }}">
                 {{ csrf_field() }}
                 <div id="msg_box_mobile"></div>
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Alamat Email" aria-describedby="basic-addon2" name="email" id="email-mobile">
                  <span class="input-group-addon" id="basic-addon2"><i class="fa fa-envelope-o"></i></span>
                </div>
                <div class="input-group">
                  <input type="password" class="form-control" placeholder="Password" aria-describedby="basic-addon2" name="password" id="password-mobile">
                  <span class="input-group-addon" id="basic-addon2"> <i class="fa fa-lock"></i> </span>
                </div>
                <input type="submit" class="btn btn-success" type="button" id="btn-login" value="Masuk"> 
                </form>
                <span id="helpBlock" class="help-block">
                  Ibu belum pernah mendaftar?
                </span>
                <button class="btn btn-success btn-daftar" type="button" id="submit-button-daftar-mobile" href="{{ route('register')}}">
                  Daftar di Sini
                </button>
            </div>
            @else
               <a href="{{ route('logout') }}">LOGOUT</a>
            @endif
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="overlay-collapsed-nav"></div>