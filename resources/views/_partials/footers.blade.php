 <footer class="ideanak-footer">
    <div class="container">
      <div class="flex-container">
        <div class="copyright">
          <p>
            Copyright <span>@ 2018 Frisian Flag.</span> All rights reserved
          </p>
        </div>
        <div class="footer-link">
          
          <a href="#terms-and-condition">Syarat dan Ketentuan</a>
          
        </div>
        <div class="footer-social-media">
          <span>Follow us:</span>
          <a href="https://www.facebook.com/Ibudanbalita">
              <i class="fa fa-facebook"></i>
          </a>
          <a href="https://twitter.com/ibudanbalita">
              <i class="fa fa-twitter"></i>
          </a>
          <a href="https://www.instagram.com/ibudanbalita/">
              <i class="fa fa-instagram"></i>
          </a>
        </div>
      </div>
    </div>
  </footer>

   <!-- Modal -->
  <div class="login-modal modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
           <a href="{{ route('user.facebook.login')}}" class="button facebook Login-facebook">
              <span>
                <i class="fa fa-facebook"></i>
              </span>
              <p>Login with Facebook</p>
            </a>
          <form id="login-form" method="post" role="form" action="{{ route('login.submit.ajax') }}">
            {{ csrf_field() }}
            <div id="msg_box"></div>
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Alamat Email" aria-describedby="basic-addon2" name="email" id="email">
              <span class="input-group-addon" id="basic-addon2"><i class="fa fa-envelope-o"></i></span>
            </div>
            <div class="input-group">
              <input id="password" type="password" class="form-control" placeholder="Password" aria-describedby="basic-addon2" name="password">
              <span class="input-group-addon" id="basic-addon2"> <i class="fa fa-lock"></i> </span>
            </div>
            <input type="submit" class="btn btn-success" type="button" id="btn-login" value="Masuk"> 
            </form>
            <span id="helpBlock" class="help-block">
              Ibu belum pernah mendaftar?
            </span>
            <a class="btn btn-success btn-daftar" type="button" id="submit" href="{{ route('register')}}"> 
              Daftar di Sini
            </a>
         
        </div>
      </div>
    </div>
  </div>

  <script src="{{ asset('assets/semangatibu/js/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/semangatibu/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/semangatibu/js/jquery.unveil.js') }}"></script>
  <script src="{{ asset('assets/semangatibu/js/site.min.js') }}"></script>

  <script type="text/javascript">
      $("#login-form").submit(function (e) {
        $("#msg_box").html('');
        var post_data = $(this).serialize();
        if($('#email').val() == '' || $("#password").val() == ''){
          $("#msg_box").html('Email & Password harus diisi');
          return false;
        }
        var url = $(this).attr('action');
        $('#btn-login').val('Please wait...');
        
        $.ajax({
          type : 'POST',
          url : url,
          dataType: 'json',
          data: post_data,
          success: function(data){
            if(data.message == 'fail'){
                $("#msg_box").html(data.error);
            }else{
              window.location.href = data.redirect;
            }
          },
        });
        return false;
      });

      $("#login-form-mobile").submit(function (e) {
        $("#msg_box_mobile").html('');
        var post_data = $(this).serialize();
        if($('#email-mobile').val() == '' || $("#password-mobile").val() == ''){
          $("#msg_box_mobile").html('Email & Password harus diisi');
          return false;
        }
        var url = $(this).attr('action');
        $('#btn-login').val('Please wait...');
        
        $.ajax({
          type : 'POST',
          url : url,
          dataType: 'json',
          data: post_data,
          success: function(data){
            if(data.message == 'fail'){
                $("#msg_box_mobile").html(data.error);
            }else{
              window.location.href = data.redirect;
            }
          },
        });
        return false;
      });

      $("#submit-button-daftar").on('click', function(){
        window.location.href = $(this).attr('href');
      });

      $(".learn-more").on('click', function(){
        window.location.href= $(this).attr('href');
      })
  </script>
