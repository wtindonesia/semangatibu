@extends('admin.layout.master')
@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Data User</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="site-search">
			      <form>
			        <i class="fa fa-search"></i>
			        <input class="form-control col-md-5" type="search" placeholder="Search name or email" name="keyword" value="{{ $keyword }}">
			      </form>
			    </div>
              <table class="table table-bordered">
                <tbody>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Email</th>
                  <th>Nama Ibu</th>
                  <th>Nama Anak</th>
                  <th>Child DOB</th>
                  <th>Phone Number</th>
                  <th>FB ID</th>
                  <th>Created At</th>
                </tr>
               	 	@foreach($users as $key=>$val)
                	<tr>
                		<td>{{ $users->firstItem() + $key }}</td>
                		<td>{{ $val->email }}</td>
                		<td>{{ $val->mom_name }}</td>
                		<td>{{ $val->child_name }}</td>
                		<td>{{ date('d-m-Y', strtotime($val->child_dob)) }}</td>
                		<td>{{ $val->phone_number }}</td>
                		<td>{{ $val->facebook_id }}</td>
                		<td>{{ date('d-m-Y H:i:s', strtotime($val->created_at)) }}</td>
                	</tr>
                		@endforeach
              		</tbody>
          		</table>
          		{{ $users->links() }}
            </div>
          </div>
		</div>
	</div>
@endsection