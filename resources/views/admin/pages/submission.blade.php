@extends('admin.layout.master')
@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Data Submission</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <div class="site-search">
                <form>
                  <i class="fa fa-search"></i>
                  <input class="form-control col-md-5" type="search" placeholder="Search name or email" name="keyword" value="{{ $keyword }}">
                </form>
              </div>
              <table class="table table-bordered">
                <tbody>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Email</th>
                  <th>Nama Ibu</th>
                  <th>Nama Anak</th>
                  <th>Child DOB</th>
                  <th>Photos</th>
                  <th style="width:5px">Photos Desc.</th>
                  <th>Upload Date</th>
                  <th>Is Visible</th>
                </tr>
               	 	@foreach($submission as $key=>$val)
                	<tr>
                		<td>{{ $submission->firstItem() + $key }}</td>
                		<td>{{ $val->user->email }}</td>
                		<td>{{ $val->user->mom_name }}</td>
                		<td>{{ $val->user->child_name }}</td>
                		<td>{{ $val->user->child_dob }}</td>
                		<td><a href="{{ route('download.file', ['filename' => $val->id]) }}" target="_blank"><img width="200" src="{{ URL::asset('/upload/photos/') }}/{{ $val->picture_submission }}" /></a></td>
                    <td style="width:5px">{{ $val->picture_description }}</td>
                    <td>{{ date('d-m-Y', strtotime($val->created_at))}}</td>
                    <td><div class="is_visible" id="is_visible_{{$val->id}}" counter="{{ $val->id }}" values="{{ $val->is_visible }}"><a style="cursor: pointer;">{{ $val->is_visible == true ? "displayed" : "not displayed"}}</a></div></td>
                	</tr>
                		@endforeach
              		</tbody>
          		</table>
          		{{ $submission->links() }}
            </div>
          </div>
		</div>
	</div>
@stop

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function(){
      $('.is_visible').on('click', function(){
        var r = confirm("Are you sure to change this?");
        if(r == true){
            var counter = $(this).attr('counter');
            var values = $(this).attr('values');
            $.get('change-is-visible?id=' + counter + '&values=' + values, function(data){
              if(data.ret == 'ok'){
                $('#is_visible_' + counter).html('<a style="cursor:pointer">' + data.vis + '</a>');
                $('#is_visible_' + counter).attr('values', data.update_values);
              }
            });
          }else{
            return false;
          }

      });
    });
  </script>
@stop