<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <!-- Optionally, you can add icons to the links -->
            <li class="active"><a href="#"><span>Link</span></a></li>
            <li><a href="{{ route('admin.datauser') }}"><span>Data User</span></a></li>
            <li><a href="{{ route('admin.datasubmission') }}"><span>Data Submission</span></a></li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>