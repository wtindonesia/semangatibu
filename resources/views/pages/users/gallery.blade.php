@extends('layouts.master')

@section('styles')
<style type="text/css">
  @media only screen and (min-device-width : 768px) and (max-device-width : 1024px){ 
    .newstyle-gallery .pattern-background .gallery .picture-position{
      width: 50%
    }
  }
</style>
@stop

@section('content')
<div class="gallery-page newstyle-gallery">
<div class="pattern-background">
      <div class="container">
        <div class="row section-title">
          <div class="col-xs-12 col-sm-8 title-text">
            <h1 class="opening-title">
              <img src="{{ asset('assets/semangatibu/images/gallery-images/gallery-icon_03.png') }}" alt="">
              GALERI
            </h1>
            @if($loggedUser == true)
            <a href="{{ route('upload.submission') }}" class="btn-kirim-cerita-lagi">
              + Kirim Cerita Lagi
            </a>
            @endif
          </div>
          <div class="col-xs-12 col-sm-4 filter-column">
            @if($loggedUser == true)
            <div class="btn-group filter-gallery" data-toggle="buttons">
              <label class="btn btn-primary" id="galeri-ibu">
                  Galeri Ibu
              </label>
              <label class="btn btn-primary active" id="galeri-show-all">
               Tampilkan Semua
              </label>
              </div>
              @endif
          </div>
        </div>
        <div class="row gallery" id="row-gallery">
          @foreach($gallery as $val)
            <div class="picture-position" counter="{{ $val['id'] }}" from='all'>
              <div class="submitted-picture">
                <div class="picture-details">
                  <a class="submitted-name" href="{{ route('user.gallery.detail', ['id_submission' => $val['id']]) }}">
                    <h3>
                      <?php $mom_low = strtolower($val['mom_name']); ?>
                       @if(strlen($mom_low) > 25)
                        {{ ucwords(substr($mom_low, 0, 25)) }} ..  
                        @else
                        {{ ucwords($mom_low) }}
                        @endif
                    </h3>
                    <p>
                      <?php
                        $low_child = strtolower($val['child_name']);
                      ?>
                      @if(strlen($low_child) > 25)
                        {{ ucwords(substr($low_child, 0, 25)) }} ..  
                      @else
                        {{ ucwords($low_child) }}
                      @endif
                      <br/> {{ ucwords($val['years'])}} Tahun
                    </p>
                  </a>
                </div>
                <a href="{{ route('user.gallery.detail', ['id_submission' => $val['id']]) }}" class="picture-frame">
                  <img class="main-img unveil" src="{{ URL::asset('/upload/photos/thumbnail') }}/{{ $val['picture_submission'] }}"
                    data-src="{{ URL::asset('/upload/photos/thumbnail') }}/{{ $val['picture_submission'] }}">
                </a>
              </div>
            </div>
          @endforeach
        </div>
        <div class="row pagination">
          <div class="col-xs-12 col-sm-4 col-sm-offset-4">
            <button class="btn btn-success" id="load-more-ajax">
              Load More
            </button>
          </div>
        </div>
      </div>
    </div>
    <div class="before-footer">
      <div class="container-fluid">
        <div class="row">
          <img class="mobile" src="{{ asset('assets/semangatibu/images/before-footer-banner.png') }}">
          <img class="desktop" src="{{ asset('assets/semangatibu/images/before-footer-banner-2.jpg') }}">

          <div class="opening-text">
            <div class="column">
              <h1>
                <img class="mobile" src="{{ asset('assets/semangatibu/images/bottom-copy-banner-mobile.png') }}" alt="Dengan Primanutri Dukung #SemangatIbu">
                <img class="desktop" src="{{ asset('assets/semangatibu/images/bottom-copy-banner.png') }}" alt="Dengan Primanutri Dukung #SemangatIbu">
              </h1>
              <button class="learn-more" href="https://www.ibudanbalita.com/artikelutama/dengan-primanutri-dukung-semangatibu">
                Cari Tahu!
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function(){
      //load more
      $( '#load-more-ajax' ).on( 'click' , function() {
        var counters = [];
        $( '.picture-position' ).each( function() {
           counters.push( $(this).attr( 'counter' ) );
        });
        var lastid = Math.min.apply(Math,counters);
        var from =  $( ".picture-position" ).last().attr('from');
        $.get('gallery-loadmore?lastid=' + lastid + '&from=' + from, function(data){
           if(data != '0'){
             $( ".picture-position" ).last().after(data);
           }else{
             $('#load-more-ajax').html('Tidak ada data baru.');
           }
        });
      });

      $('#galeri-ibu').on('click', function(){
        $("#galeri-ibu").addClass('active');
        $("#galeri-show-all").removeClass('active');
        $.get('gallery-only-me', function(data){
            $('#load-more-ajax').html('Load More');
            $( ".picture-position" ).remove();  
            if(data != '0'){
              $( "#row-gallery" ).append(data);
            }
        });
      });
    $('#galeri-show-all').on('click', function(){
        $("#galeri-show-all").addClass('active');
        $("#galeri-ibu").removeClass('active');
        $.get('gallery-all', function(data){
           $('#load-more-ajax').html('Load More');
            $( ".picture-position" ).remove();  
            if(data != '0'){
              $( "#row-gallery" ).append(data);
            }
        });
      });



    });
  </script>
@stop