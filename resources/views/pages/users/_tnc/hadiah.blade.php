<b>HADIAH KOMPETISI</b>

<br/><br/>

Sebagai persembahan dari PT Frisian Flag Indonesia, pemenang #SemangatIbuPintar dari setiap periode masing-masing berhak mendapatkan Produk Frisian Flag 123 selama 6 (enam) Bulan. Perhitungan hadiah berupa Produk Frisian Flag 123 selama 6 bulan ini adalah sebagai berikut:
Rekomendasi konsumsi susu adalah 3 (tiga) gelas per hari selama 30 (tiga puluh) hari. Maka perhitungan konsumsi susu per bulannya adalah: 30 hari x 3 gelas x 40 gram per gelas = 3.600 gram = 9 boks kemasan ukuran 400 gram. Jadi, tiap pemenang mendapatkan 54 boks Frisian Flag 123 ukuran 400 gram.

<br/>
<br/>
<br/>

<b>KRITERIA PEMENANG</b>

<br/><br/>

<ol>
	<li>Pihak Penyelenggara akan memilih 5 (lima) pemenang untuk masing-masing periode.</li>
	<li>Pemenang yang dipilih oleh pihak Penyelenggara dipertimbangkan berdasarkan foto dan cerita orisinal yang paling kreatif dan paling sesuai dengan tema yang diajukan pihak penyelenggara</li>
	<li>Pengumuman pemenang akan dilakukan pada 14 Desember 2018.</li>
	<li>Pihak Penyelenggara akan melakukan validasi data Ibu melalui telepon:</li>
	<ul>
		<li>Pastikan nomor handphone/telepon yang Ibu berikan dapat dihubungi setiap waktu. </li>
		<li>Pihak Penyelenggara akan mengonfirmasi dan mencocokkan data Ibu dan si Kecil. Data Ibu dan si Kecil yang diikutsertakan saat registrasi harus sesuai dengan KTP dan Kartu Keluarga. Untuk membuktikan hal tersebut, Ibu wajib mengirimkan bukti copy scan KTP, Kartu Keluarga serta dokumentasi lain yang diperlukan ke: <br/>
Email: ibudanbalita@frieslandcampina.com <br/>
Fax: (021) 29957488
 		</li>
 		<li>
 			Pastikan data diri yang diberikan sudah benar . Pihak Penyelenggara tidak bertanggung jawab apabila Ibu melakukan kekeliruan dalam memasukkan data atau jika tim kami tidak dapat menghubungi Ibu. Setiap peserta menjamin bahwa setiap informasi yang diberikan adalah benar dan tepat.
 		</li>
	</ul>
	<li>Hadiah akan dikirim selambat-lambatnya 60 (enam puluh) hari setelah verifikasi pemenang oleh pihak Penyelenggara. Proses serah terima hadiah akan diinformasikan kembali oleh pihak Penyelenggara.</li>
	<li>PT Frisian Flag Indonesia berhak mendiskualifikasi Ibu dan si Kecil yang terbukti pernah atau sedang terikat kontrak dan/atau pernah menjadi pemenang kompetisi yang disponsori oleh produsen susu selain PT Frisian Flag Indonesia.</li>
	<li>Jika diketahui oleh PT Frisian Flag Indonesia bahwa calon pemenang maupun pemenang melakukan kecurangan atau melanggar ketentuan pada Syarat & Ketentuan ini, PT Frisian Flag Indonesia berhak untuk tidak meloloskan calon pemenang maupun pemenang tersebut tanpa pemberitahuan apa pun dan menggantinya dengan calon pemenang maupun pemenang lainnya.</li>
	<li>Keputusan Penyelenggara selaku dewan juri bersifat mutlak dan tidak dapat diganggu gugat.</li>
</ol>