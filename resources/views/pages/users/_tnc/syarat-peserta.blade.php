<ol>
	<li>Kompetisi ini diselenggarakan oleh PT Frisian Flag Indonesia – Ibu & Balita (“Penyelenggara”), pada 22 Oktober 2018 – 2 Desember 2018.</li>
	<li>Kompetisi terbuka bagi: </li>
	<ul>
		<li>Ibu yang memiliki anak usia 1-3 tahun dan mendaftarkan dirinya di situs #SemangatIbuPintar (www.ibudanbalita.com/semangat-ibu-pintar).  </li>
		<li>Warga negara Indonesia yang berdomisili di wilayah Republik Indonesia</li>
	</ul>
	<li>
		Bukan merupakan “pemburu hadiah/quiz hunter”, karyawan, keluarga inti karyawan (ayah/ibu/istri/suami/anak) PT Frisian Flag Indonesia (FFI), dan/atau pihak ketiga yang bekerja sama dengan FFI yang terlibat dalam penyelenggaraan kompetisi ini termasuk didalamnya supplier packaging, distributor, agency dan karyawan dari pihak-pihak tersebut.
	</li>
	<li>
		Ibu dan si Kecil <b>tidak diperkenankan</b> mengikuti kompetisi #SemangatIbuPintar, jika:
	</li>
	<ul>
		<li>Pernah menjadi bintang iklan produk lain selain PT Frisian Flag Indonesia setidaknya 12 bulan sebelum cerita dan foto dikirimkan untuk kompetisi ini, baik secara perseorangan maupun berkelompok.</li>
		<li>Pernah atau sedang terikat kontrak dengan perusahaan merek susu lain selain PT. Frisian Flag Indonesia dan belum berakhir hingga #SemangatIbuPintar berlangsung.</li>
	</ul>
	<li>Semua materi yang diikutsertakan dalam kompetisi #SemangatIbuPintar menjadi hak milik eksklusif PT Frisian Flag Indonesia dan dapat digunakan untuk materi komunikasi atau promosi PT Frisian Flag Indonesia dengan atau tanpa pemberitahuan terlebih dahulu. Setiap peserta kompetisi setuju jika data yang masuk akan disimpan dan akan digunakan untuk kepentingan kompetisi oleh pihak penyelenggara. </li>
	<li>PT Frisian Flag Indonesia menyelenggarakan kompetisi ini didasarkan atas itikad baik, peserta juga harus beritikad baik dalam berpartisipasi dalam kompetisi ini. Untuk itu, Penyelenggara berhak dan setiap saat, memiliki kewenangan mutlak untuk mendiskualifikasikan peserta yang dianggap tidak beritikad baik, termasuk diantaranya:</li>
	<ul>
		<li>Dikategorikan oleh PT Frisian Flag Indonesia atau agency yang ditunjuk sebagai “pemburu hadiah/quiz hunter”;</li>
		<li>Mencemarkan nama baik, melanggar, memfitnah, merendahkan, melecehkan, mengancam Penyelenggara;</li>
		<li>Melakukan kecurangan, penipuan, pemalsuan dan memberikan informasi yang tidak benar dan/atau menyesatkan;</li>
		<li>Melakukan tindakan yang menurut kebijakan Penyelenggara merupakan tindakan yang bertentangan dengan kesusilaan, kesopanan, kepatutan, dan/atau kebiasaan baik yang berlaku; dan/atau</li>
		<li>Melakukan tindakan yang melanggar atau bertentangan dengan ketentuan peraturan perundang-undangan yang berlaku di Indonesia.</li>
	</ul>
</ol>