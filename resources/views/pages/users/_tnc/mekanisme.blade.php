<ol>
	<li>Ibu dapat mulai mengikuti kompetisi #SemangatIbuPintar melalui langkah-langkah berikut:</li>
	<ul>
		<li>Daftarkan diri Ibu dan si Kecil melalui situs www.ibudanbalita.com/semangat-ibu-pintar </li>
		<li>Mohon masukkan data Ibu dan si Kecil dengan benar dan lengkap, sesuai data diri di KTP dan Kartu Keluarga. </li>
		<li>Ibu dapat mendaftarkan maksimal 4 (empat) anak dalam satu akun. </li>
	</ul>
	<li>Kirim foto dan cerita #SemangatIbuPintar dengan cara:</li>
	<ul>
		<li>Unggah foto dan cerita tentang momen, kejutan cerdas, keaktifan, maupun pertumbuhan fisik si Kecil.  </li>
		<li>Batas panjang cerita maksimal 400 (empat ratus) karakter untuk tiap foto.</li>
		<li>1 (satu) cerita harus dilengkapi 1 (satu) foto. Ukuran foto yang dikirim tidak boleh lebih dari 3 (tiga) Megabyte.</li>
		<li>Foto yang dikirim harus menggambarkan cerita yang disampaikan.</li>
		<li>Cerita dan foto yang dikirimkan tidak boleh mengandung unsur SARA dan pornografi, serta tidak menunjukkan bayi di bawah 12 bulan dan pemberian susu formula untuk bayi di bawah usia 1 (satu) tahun.</li>
		<li>Cerita yang dikirimkan harus merupakan kisah nyata si Kecil dan belum pernah dipublikasikan atau diikutsertakan dalam kegiatan pemasaran kompetisi (merek atau perusahaan susu) selain PT. Frisian Flag Indonesia.</li>
		<li>Foto dan cerita yang dikirimkan belum pernah diikutsertakan di kompetisi lainnya di periode sebelumnya dan tidak melanggar hak cipta pihak mana pun.</li>
		<li>Setiap foto dan cerita yang dikirimkan akan ditinjau kembali oleh pihak PT. Frisian Flag Indonesia.</li>
	</ul>
	<li>Foto dan cerita yang sudah dikirimkan tidak dapat diganti. Namun, Ibu dapat mengirimkan lebih dari 1 (satu) foto dan cerita.</li>
</ol>