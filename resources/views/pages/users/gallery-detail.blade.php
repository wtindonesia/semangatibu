@extends('layouts.master')

@section('content')
<div class="gallery-detail newstyle-gallery-detail">
    <div class="pattern-background">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-offset-1">
            <a href="{{ route('user.gallery')}}" class="back-to-gallery">
              < Kembali ke Galeri
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-4 col-sm-offset-1">
            <div class="picture-frame">
                <img class="main-img unveil" src="{{ URL::asset('/upload/photos/thumbnail') }}/{{ $gallery['picture_submission'] }}"
                data-src="{{ URL::asset('/upload/photos/thumbnail') }}/{{ $gallery['picture_submission'] }}">
            </div>
          </div>
          <div class="col-xs-12 col-sm-6">
            <div class="row-flex">
              <div class="column submitted-detail">
                <h3 class="submitted-mother">
                  <?php $mom_low = strtolower($gallery['mom_name']); ?>
                  {{ ucwords($mom_low) }}
                </h3>
                <p class="submitted-name">
                  <?php $child_low = strtolower($gallery['child_name']); ?>
                  {{ ucwords($child_low) }}, {{ $gallery['years'] }} Tahun
                </p>
                <p class="submitted-story">
                    {{ $gallery['picture_description'] }}
                </p>
              </div>
              <div class="column share-section">
                <span>
                  Share
                </span>
                &nbsp;                          
                 <a target="_blank" href="https://api.whatsapp.com/send?text=<?= $washare; ?>">
                  <i class="fa fa-whatsapp" aria-hidden="true"></i>
                </a>
                &nbsp;
                <a target="_blank" href="https://web.facebook.com/sharer/sharer.php?u={{ htmlentities( request()->fullUrl() ) }}&_rdc=1&_rdr">
                <i class="fa fa-facebook" aria-hidden="true"></i>
                 &nbsp;
                 <a target="_blank" href="https://twitter.com/intent/tweet?url={{ request()->fullUrl() }}&text={{ htmlentities('Dengan Primanutri Dukung #SemangatIbu ') }}">
                <i class="fa fa-twitter" aria-hidden="true"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="before-footer">
    <div class="container-fluid">
      <div class="row">
        <img class="mobile" src="{{ asset('assets/semangatibu/images/before-footer-banner.png') }}">
        <img class="desktop" src="{{ asset('assets/semangatibu/images/before-footer-banner-2.jpg') }}">

        <div class="opening-text">
          <div class="column">
            <h1>
              <img class="mobile" src="{{ asset('assets/semangatibu/images/bottom-copy-banner-mobile.png') }}" alt="Dengan Primanutri Dukung #SemangatIbu">
              <img class="desktop" src="{{ asset('assets/semangatibu/images/bottom-copy-banner.png') }}" alt="Dengan Primanutri Dukung #SemangatIbu">
            </h1>
            <button class="learn-more" href="https://www.ibudanbalita.com/artikelutama/dengan-primanutri-dukung-semangatibu">
              Cari Tahu!
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
@stop