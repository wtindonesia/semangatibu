@extends('layouts.master')

@section('content')

<div class="register new-style">
  <div class="container">
    <div class="row desktop-flex">
      <div class="col-xs-12 col-sm-5">
        <div class="section-title">
          <img class="mobile" src="{{ asset('assets/semangatibu/images/register-images/register-mobile-section-title_03.png') }}" alt="">
          <img class="desktop" src="{{ asset('assets/semangatibu/images/register-images/register-section-bg_03.jpg') }}" alt="">
          <h1>
            Register
          </h1>
        </div>
      </div>
      <div class="col-xs-12 col-sm-7">
        <div class="form-wrapper">
          @if(!$is_facebook_session)
            <a href="{{ route('user.facebook.login')}}" class="button facebook Login-facebook">
              <span>
                <i class="fa fa-facebook"></i>
              </span>
              <p>Login with Facebook</p>
            </a>
          @else
            Silahkan lengkapi data Anda.
          @endif
          <div class="row">
            <div class="col-xs-12">
              <div class="card ">
                <div class="card-body">
                  <form class="row" {{ request()->fullUrl() }}" method="post" >
                    {{ csrf_field() }}
                    <input type="hidden" name="facebook_id" id="facebook_id" value="{{ $input['facebook_id'] }}" />
                    <div class="form-group col-xs-12 col-md-6">
                      <label for="nama-ibu"> Nama Ibu</label>
                      <input class="form-control" placeholder="Sesuai KTP" type="text" id="nama-ibu" name="nama_ibu" value="{{ $input['nama_ibu'] }}" />
                         @if ($errors->has('nama_ibu'))
                          @foreach( $errors->get('nama_ibu') as $msg_mom )
                            <span id="" class="help-block help-block-alert label label-danger">{{$msg_mom}}</span>
                          @endforeach
                        @endif
                    </div>
                    <div class="col-xs-12">
                      <div class="row">
                        <div class="form-group col-xs-12 col-md-6">
                          <label for="nama-anak">Nama Anak</label>
                          <input class="form-control" placeholder="Sesuai Kartu Keluarga" type="text" id="nama-anak" name="nama_anak" value="{{ $input['nama_anak'] }}"/>
                          @if ($errors->has('nama_anak'))
                            @foreach( $errors->get('nama_anak') as $msg_mom )
                              <span id="" class="help-block help-block-alert label label-danger">{{$msg_mom}}</span>
                            @endforeach
                          @endif
                        </div>
                        <div class="form-group col-xs-12 col-md-6">
                          <div class="input-group input-append date">
                            <label for="tanggal-lahir-anak"> Tanggal Lahir Anak</label>
                            <input readonly="true" type="text" class="form-control" name="tanggal_lahir_anak" id="tanggal-lahir-anak" value="{{ $input['tanggal_lahir_anak'] }}" placeholder="dd/mm/YYYY" />
                             @if ($errors->has('tanggal_lahir_anak'))
                              @foreach( $errors->get('tanggal_lahir_anak') as $msg_mom )
                                <span id="" class="help-block help-block-alert label label-danger">{{$msg_mom}}</span>
                              @endforeach
                            @endif
                          </div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6">
                          <label for="email-ibu">Email</label>
                          <input class="form-control" placeholder="Mohon masukkan email yang masih aktif" type="email" id="email-ibu" name="email_ibu" value="{{ $input['email_ibu'] }}" />
                          @if ($errors->has('email_ibu'))
                              @foreach( $errors->get('email_ibu') as $msg_mom )
                                <span id="" class="help-block help-block-alert label label-danger">{{$msg_mom}}</span>
                              @endforeach
                            @endif
                        </div>
                        <div class="form-group col-xs-12 col-md-6">
                          <label for="nomor-handphone">Nomor Handphone</label>
                          <input class="form-control" placeholder="Mohon masukkan nomor yang masih aktif" type="tel" id="nomor-handphone" name="nomor_handphone" autocomplete="off" value="{{ $input['nomor_handphone'] }}" />
                          @if ($errors->has('nomor_handphone'))
                              @foreach( $errors->get('nomor_handphone') as $msg_mom )
                                <span id="" class="help-block help-block-alert label label-danger">{{$msg_mom}}</span>
                              @endforeach
                            @endif
                        </div>
                        <div class="form-group col-xs-12 col-md-6">
                          <label for="hash">Password</label>
                          <input class="form-control" placeholder="Password harus terdiri huruf kapital, huruf kecil, angka, dan karakter alfanumerik" type="password" id="hash" name="password" />
                           @if ($errors->has('password'))
                              @foreach( $errors->get('password') as $msg_mom )
                                <span id="" class="help-block help-block-alert label label-danger">{{$msg_mom}}</span>
                              @endforeach
                            @endif

                        </div>
                        <div class="form-group col-xs-12 col-md-6">
                          <label for="hash-verify">Ulangi Password</label>
                          <input class="form-control" placeholder="Password yang harus diketik sama dengan password di atas" type="password" id="password_confirmation" name="password_confirmation" />
                           @if ($errors->has('password.regex'))
                            @foreach( $errors->get('password.regex') as $msg_passwordregex )
                              <span id="" class="help-block help-block-alert label label-danger">{{$msg_passwordregex}}</span>
                            @endforeach
                          @endif
                        </div>
                        <div class="form-group col-xs-12">

                          <span id="helpBlock" class="help-block">
                            Peserta bersedia apabila datanya digunakan oleh penyelenggara untuk kepentingan kompetisi, materi komunikasi, maupun promosi. Selengkapnya baca Syarat & Ketentuan.
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group col-xs-12">
                      <button class="btn btn-success" type="submit" id="submit"> 
                        Selanjutnya
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@stop