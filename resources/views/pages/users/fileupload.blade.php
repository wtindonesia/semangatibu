@extends('layouts.master')

@section('content')

@section('styles')
    <style type="text/css">
        .submit-photo-page.new-style .section-title .photo-upload-box .image-preview{
            width: 100%;
        }
    </style>
@stop

<div class="submit-photo-page new-style">
    <div class="container">
        <form action="{{route('upload.submission.submit')}}" method="post" enctype="multipart/form-data"  class="row desktop-flex">
            {{ csrf_field() }}
            <div class="col-xs-12 col-sm-5">
                <div class="section-title">
                    <div class="img-wrapper">
                        <img class="mobile" src="{{ asset('assets/semangatibu/images/register-images/title-section-submit-mobile_03.png') }}"
                        alt="">
                        <img class="desktop" src="{{ asset('assets/semangatibu/images/register-images/title-section-submit-bg_03.jpg') }}" alt="">
                        <h1>
                            <span>Submit</span> Foto
                        </h1>
                    </div>
                    <div class="photo-upload-box" id="photo-upload-box">
                        <img src="{{ asset('assets/semangatibu/images/register-images/camera.png') }}" alt="" id="image-preview">
                        <label class="btn btn-upload" for="picture_submission">
                            Upload Foto
                            <input type="file" name="picture_submission" id="picture_submission" />
                        </label>
                    </div>
                    @if ($errors->has('picture_submission'))
                        @foreach( $errors->get('picture_submission') as $msg_video )
                          <span id="" class="help-block help-block-alert label label-danger">{{$msg_video}}</span>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="col-xs-12 col-sm-7">
                <div class="form-wrapper">
                    <div class="row">
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                            <div class="row">
                                <div class="form-group col-xs-12">
                                    <label for="moment-text">
                                        Ceritakan momen, keaktifan,<span> maupun pertumbuhan si Kecil yang menjadi penyemangat Ibu.</span>
                                    </label>
                                    <textarea class="form-control" name="picture_description" id="picture_description" rows="12" placeholder="(maksimal 400 karakter) #SemangatIbuPintar" id="momen-text" name="momen-text" autocomplete="off" ></textarea>
                                      @if ($errors->has('picture_description'))
                                        @foreach( $errors->get('picture_description') as $msg_video )
                                          <span id="" class="help-block help-block-alert label label-danger">{{$msg_video}}</span>
                                        @endforeach
                                    @endif
                                </div>
                                <div class="form-group col-xs-12">
                                    <button class="btn btn-success" type="submit" id="submit"> 
                                        Kirim
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@stop
@section('scripts')
    <script type="text/javascript">
    
    function readURL(input) {

     if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#image-preview').attr('src','');
          $('#image-preview').addClass('image-preview');
          $('#image-preview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }
    $(document).ready(function(){;
        $("#picture_submission").change(function() {
            readURL(this);
        });  
    });
    </script>

@stop