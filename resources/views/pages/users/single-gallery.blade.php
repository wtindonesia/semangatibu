@foreach($gallery as $val)
<div class="picture-position" counter="{{ $val['id'] }}" from='{{ $from }}'>
  <div class="submitted-picture">
    <div class="picture-details">
      <a class="submitted-name" href="{{ route('user.gallery.detail', ['id_submission' => $val['id']]) }}">
       <h3>
        <?php $mom_low = strtolower($val['mom_name']); ?>
        @if(strlen($mom_low) > 25)
        {{ ucwords(substr($mom_low, 0, 25)) }} ..  
        @else
        {{ ucwords($mom_low) }}
        @endif
      </h3>
      <p>
        <?php
        $low_child = strtolower($val['child_name']);
        ?>
        @if(strlen($low_child) > 25)
        {{ ucwords(substr($low_child, 0, 25)) }} ..  
        @else
        {{ ucwords($low_child) }}
        @endif
        <br/> {{ ucwords($val['years'])}} Tahun
      </p>
    </a>
  </div>
  <a href="{{ route('user.gallery.detail', ['id_submission' => $val['id']]) }}" class="picture-frame">
    <img class="main-img unveil" src="{{ URL::asset('/upload/photos/thumbnail') }}/{{ $val['picture_submission'] }}"
    data-src="{{ URL::asset('/upload/photos/thumbnail') }}/{{ $val['picture_submission'] }}">
  </a>
</div>
</div>
@endforeach