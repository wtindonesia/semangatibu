@extends('layouts.master')

@section('content')
<div class="thank-you-page new-style">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="section-title">
					<div class="img-wrapper">
						<img class="mobile" src="{{ asset('assets/semangatibu/images/register-images/title-section-submit-mobile_03.png') }}" alt="">
						<h1>
							<span>Terima</span> Kasih
						</h1>
						<img class="balloon" src="{{ asset('assets/semangatibu/images/terimakasih-balloon_03.png') }}" alt="">
					</div>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="column-thanks">
					<h3 class="thankyou-heading">
						Foto dan cerita si Kecil telah berhasil didaftarkan dalam kompetisi #SemangatIbuPintar
					</h3>
					<span class="line-separator"></span>
					<p class="thankyou-paragraph">
						Untuk memperbesar kesempatan memenangkan hadiah, Ibu bisa mengirim foto dan cerita sebanyak-banyaknya dengan tema Penyemangat Hariku selama periode kompetisi berlangsung. <b>Cari tahu juga informasi untuk mendukung si Kecil tumbuh sesuai usianya.</b>
					</p>
					<a href="{{ route('user.gallery') }}" class="btn btn-success" type="button" id="submit"> 
						Lihat Galeri
					</a>
					<a href="https://www.ibudanbalita.com/artikelutama/semangat-ibu-pintar" class="btn btn-success btn-read" type="button" id="submit"> 
						Baca Artikel
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

@stop