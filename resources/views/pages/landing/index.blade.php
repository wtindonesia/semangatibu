@extends('layouts.master')

@section('styles')
<style>
  .ideanak-content.new-style .before-footer .opening-text a.learn-more {
    background: #fcbc12;
    background: -moz-linear-gradient(180deg, #fcbc12 0%, #fcbc12 0%, #fbad0e 100%);
    background: -webkit-linear-gradient(180deg, #fcbc12 0%, #fcbc12 0%, #fbad0e 100%);
    background: linear-gradient(180deg, #fcbc12 0%, #fcbc12 0%, #fbad0e 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#2668a8",endColorstr="#fbad0e",GradientType=1);
    text-transform: uppercase;
    color: white;
    font-weight: 700;
    font-size: 10px;
    padding: 2px 25px;
    border-radius: 4px;
    border: 0;
    display: inline-block
  }

  @media screen and (min-width: 768px) {
    .ideanak-content.new-style .before-footer .opening-text a.learn-more {
        margin-top: 1em;
        font-size: 16px;
        padding: 0.5em 3em;
    }
  }
</style>

@stop

@section('content')
<div class="ideanak-content new-style">
  <div class="top-banner-section">
    <div class="banner-img">
      <img class="products-img kotaksusu" src="{{ asset('assets/semangatibu/images/mobile-kotaksusu.png') }}">
      <img class="mobile-ideanak visible-xs-block" src="{{ asset('assets/semangatibu/images/mobile-ideanak.png') }}">
      <img class="ideanak-banner hidden-xs" src="{{ asset('assets/semangatibu/images/ideanak-banner.png') }}">
    </div>
    <div class="top-banner-copy">
      <div class="opening-text">
        <h1>
          <img src="{{ asset('assets/semangatibu/images/top-copy-banner.png') }}" alt="AYO IKUTI #SEMANGATIBUPINTAR!">
        </h1>
        <p>Susu pertumbuhan Frisian Flag 123 mengajak Ibu untuk berbagi foto dan cerita seputar aksi si kecil yang menunjukkan ia <b>cepat tangkap, aktif bergerak, dan tumbuh sesuai usianya.</b></p>
      </div>
      <a href="#" class="video-player" data-youtube-src="https://www.youtube.com/embed/JjAL2LxmAD0?autoplay=1">
        <div class="video-player-img">
          <img src="{{ asset('assets/semangatibu/images/mobile-player-video-mini.jpg') }}" alt="">
          <i class="fa fa-play" aria-hidden="true"></i>
        </div>
        <div class="video-player-copy">
          <span>
            Putar video
          </span>
        </div>
      </a>
    </div>
  </div>
  <div class="hadiah-section">
    <div class="container">
      <div class="row">
        <div class="section-title-wrapper">
          <span class="line hidden-xs"></span>
          <h1 class="title section-title">
            Hadiah
            <span class="line"></span>
          </h1>
          <span class="line hidden-xs"></span>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 hadiah-content">
          <div class="row hadiah-box">
            <img class="mobile-banner-hadiah" src="{{ asset('assets/semangatibu/images/mobile-banner-hadiah-bg-2.png') }}">
            <div class="col-xs-12 col-sm-6 align-center">
              <img class="kotak-susu" src="{{ asset('assets/semangatibu/images/kotak-susu-hadiah_03.png') }}">
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="row">
                <div class="col-xs-12">
                  <div class="hadiah-box__banner">
                    <h3>
                      Produk Frisian Flag Selama 6 Bulan
                    </h3>
                  </div>
                </div>
                <div class="col-xs-12">
                  <h3 class="hadiah-box__below-banner">
                    untuk total 10 pemenang
                  </h3>
                </div>
                <div class="col-xs-12">
                  <h5 class="hadiah-box__periode-heading">
                    Periode 1 untuk 5 pemenang
                  </h5>
                  <h5 class="hadiah-box__periode-date">
                    20 Oktober 2018 - 11 November 2018
                  </h5>
                  <h5 class="hadiah-box__periode-heading">
                    Periode 2 untuk 5 pemenang
                  </h5>
                  <h5 class="hadiah-box__periode-date">
                    12 November 2018 - 2 Desember 2018
                  </h5>
                </div>

              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <div class="how-to-join-section">
    <div class="container">
      <div class="row">
        <div class="section-title-wrapper">
          <span class="line hidden-xs"></span>
          <h1 class="title section-title">
            Cara Ikutan
            <span class="line"></span>
          </h1>
          <span class="line hidden-xs"></span>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 how-to-join-content">
          <ul class="how-to-join__slider" data-slick='{"slidesToShow": 1, "slidesToScroll": 1}'>
            <li>
              <div>
                <h5>
                  Langkah <img src="{{ asset('assets/semangatibu/images/how-to-join-count-1.png') }}" class="slider-count"
                    alt=""> <span>dari 3</span>
                </h5>
                <img src="{{ asset('assets/semangatibu/images/how-to-join-item-1.png') }}" alt="">
              </div>
              <h3 class="how-to-join__copy">
                Daftarkan diri
              </h3>
            </li>
            <li class="arrow hidden-xs">
              <img src="{{ asset('assets/semangatibu/images/green-arrow.png') }}" alt="">
            </li>
            <li>
              <div>
                <h5>
                  Langkah <img src="{{ asset('assets/semangatibu/images/how-to-join-count-2.png') }}" class="slider-count"
                    alt=""> <span>dari 3</span>
                </h5>
                <img src="{{ asset('assets/semangatibu/images/how-to-join-item-2.png') }}" alt="">
              </div>
              <h3 class="how-to-join__copy">
                Unggah foto
              </h3>
            </li>
            <li class="arrow hidden-xs">
              <img src="{{ asset('assets/semangatibu/images/green-arrow.png') }}" alt="">
            </li>
            <li>
              <div>
                <h5>
                  Langkah <img src="{{ asset('assets/semangatibu/images/how-to-join-count-3.png') }}" class="slider-count"
                    alt=""> <span>dari 3</span>
                </h5>
                <img src="{{ asset('assets/semangatibu/images/how-to-join-item-3.png') }}" alt="">
              </div>
              <h3 class="how-to-join__copy">
                Kirim Cerita
              </h3>
            </li>
          </ul>
        </div>
        <div class="col-xs-12 col-sm-4 col-sm-offset-4">
          @if($loggedUser == false)
            <a href="{{ route('register') }}" class="btn btn-success btn-daftar">Submit Foto</a>
          @else
            <a href="{{ route('upload.submission') }}" class="btn btn-success btn-daftar">Submit Foto</a>
          @endif
        </div>
      </div>
    </div>
  </div>
  <div class="terms-condition-section" id="terms-and-condition">
    <div class="container">
      <div class="row">
        <div class="section-title-wrapper">
          <span class="line hidden-xs"></span>
          <h1 class="title section-title">
            Syarat & Ketentuan
            <span class="line"></span>
          </h1>
          <span class="line hidden-xs"></span>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 terms-condition-content">
          <div class="hidden-xs">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active">
                <a href="#persyaratan-peserta" aria-controls="persyaratan-peserta" role="tab" data-toggle="tab">Persyaratan
                  Peserta</a>
              </li>
              <li role="presentation">
                <a href="#mekanisme" aria-controls="mekanisme" role="tab" data-toggle="tab" class="info-mekanisme">Mekanisme</a>
              </li>
              <li role="presentation">
                <a href="#hadiah-dan-pemenang" aria-controls="hadiah-dan-pemenang" role="tab" data-toggle="tab" class="info-hadiah">Hadiah
                  Dan Pemenang</a>
              </li>
              <li role="presentation">
                <a href="#lain-lain" aria-controls="lain-lain" role="tab" data-toggle="tab" class="info-lainlain">Lain-lain</a>
              </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="persyaratan-peserta">
                <p>
                 @include('pages.users._tnc.syarat-peserta')
                </p>
              </div>
              <div role="tabpanel" class="tab-pane" id="mekanisme">
                <p>
                  @include('pages.users._tnc.mekanisme')
                </p>
              </div>
              <div role="tabpanel" class="tab-pane" id="hadiah-dan-pemenang">
                <p>
                  @include('pages.users._tnc.hadiah')
                </p>
              </div>
              <div role="tabpanel" class="tab-pane" id="lain-lain">
                <p>
                  @include('pages.users._tnc.lainlain')
                </p>
              </div>
            </div>

          </div>
          <div class="panel-group visible-xs" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingTerms">
                <h4 class="panel-title">
                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#terms-accordion"
                    aria-expanded="false" aria-controls="collapseTerms">
                    Persyaratan Peserta
                    <span class="fa fa-chevron-down" aria-hidden="true"></span>
                  </a>
                </h4>
              </div>
              <div id="terms-accordion" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTerms">
                <div class="panel-body">
                  @include('pages.users._tnc.syarat-peserta')
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingMekanisme">
                <h4 class="panel-title">
                  <a class="collapsed info-mekanisme" role="button" data-toggle="collapse" data-parent="#accordion" href="#mekanisme-accordion"
                    aria-controls="collapseMekanisme" >
                    Mekanisme
                    <span class="fa fa-chevron-down" aria-hidden="true"></span>
                  </a>
                </h4>
              </div>
              <div id="mekanisme-accordion" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingMekanisme">
                <div class="panel-body">
                  @include('pages.users._tnc.mekanisme')
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingHadiah">
                <h4 class="panel-title">
                  <a class="collapsed info-hadiah" role="button" data-toggle="collapse" data-parent="#accordion" href="#hadiah-accordion"
                    aria-expanded="false" aria-controls="collapseHadiah">
                    Hadiah dan Pemenang
                    <span class="fa fa-chevron-down" aria-hidden="true"></span>
                  </a>
                </h4>
              </div>
              <div id="hadiah-accordion" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingHadiah">
                <div class="panel-body">
                  @include('pages.users._tnc.hadiah')
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingOthers">
                <h4 class="panel-title">
                  <a class="collapsed info-lainlain" role="button" data-toggle="collapse" data-parent="#accordion" href="#others-accordion"
                    aria-expanded="false" aria-controls="collapseOthers">
                    Lain-lain
                    <span class="fa fa-chevron-down" aria-hidden="true"></span>
                  </a>
                </h4>
              </div>
              <div id="others-accordion" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOthers">
                <div class="panel-body">
                  @include('pages.users._tnc.lainlain')
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="before-footer">
    <div class="container-fluid">
      <div class="row">
        <img class="mobile" src="{{ asset('assets/semangatibu/images/before-footer-banner.png') }}">
        <img class="desktop" src="{{ asset('assets/semangatibu/images/before-footer-banner-2.jpg') }}">

        <div class="opening-text">
          <div class="column">
            <h1>
              <img class="mobile" src="{{ asset('assets/semangatibu/images/bottom-copy-banner-mobile.png') }}" alt="Dengan Primanutri Dukung #SemangatIbu">
              <img class="desktop" src="{{ asset('assets/semangatibu/images/bottom-copy-banner.png') }}" alt="Dengan Primanutri Dukung #SemangatIbu">
            </h1>
            <a class="learn-more" href="https://www.ibudanbalita.com/artikelutama/semangat-ibu-pintar">
              Cari Tahu!
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade video-modal" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe width="853" height="480" frameborder="0" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
  @if($loggedUser == false)
      <a href="{{ route('register') }}" class="register-button">Submit Foto!</a>
    @else
       <a href="{{ route('upload.submission') }}" class="register-button">Submit Foto!</a>
    @endif
</div>
@stop

