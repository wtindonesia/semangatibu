<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Semangat Ibu Pintar 2018 Frisian Flag</title>

  <meta name="description" content="Kirim cerita &amp; foto #SemangatIbuPintar si Kecil, raih kesempatan jadi bintang iklan Frisian Flag &amp; hadiah puluhan juta rupiah!">

  <meta name="twitter:card" content="summary">
  <!-- Twitter handle of the site -->
  <meta name="twitter:site" content="">

  <meta property="fb:app_id" content="1439498536365603">

  <meta property="og:site_name" content="Semangat Ibu Pintar 2018">
  <meta property="og:url" content="{{ request()->fullUrl() }}">
  <meta property="og:type" content="website">
  <meta property="og:title" content=" Ayo Ikuti #SemangatIbuPintar!
Susu pertumbuhan Frisian Flag 123 mengajak Ibu untuk berbagi foto dan cerita seputar aksi si kecil yang menunjukkan ia cepat tangkap, aktif bergerak, dan tumbuh sesuai usianya.">
  <meta property="og:description" content="Kirim cerita &amp; foto #SemangatIbuPintar si Kecil, raih kesempatan jadi bintang iklan Frisian Flag &amp; hadiah puluhan juta rupiah!">
  <meta property="og:image" content="{{ asset('assets/semangatibu/images/mobile-ideanak-min.png') }}">

  <link rel="canonical" href="{{ request()->fullUrl() }}">

  <meta name="base_url" content="{{ request()->fullUrl() }}">
  <meta name="_token" content="F2OmeM35amuRxsGmGbCucs41k7G8DAgDddMmg8BJ">

  {{-- <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/semangatibu/images/mobile-ideanak-min.png') }}">
  <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/semangatibu/images/mobile-ideanak-min.png') }}">
  <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/semangatibu/images/mobile-ideanak-min.png') }}">
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/semangatibu/images/mobile-ideanak-min.png') }}">
  <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/semangatibu/images/mobile-ideanak-min.png') }}">
  <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/semangatibu/images/mobile-ideanak-min.png') }}">
  <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/semangatibu/images/mobile-ideanak-min.png') }}">
  <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/semangatibu/images/mobile-ideanak-min.png') }}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/semangatibu/images/mobile-ideanak-min.png') }}">
  <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('assets/semangatibu/images/mobile-ideanak-min.png') }}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/semangatibu/images/mobile-ideanak-min.png') }}">
  <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/semangatibu/images/mobile-ideanak-min.png') }}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/semangatibu/images/mobile-ideanak-min.png') }}"> --}}

   <link rel="apple-touch-icon" sizes="57x57" href="https://www.ibudanbalita.com/momenwow/assets/images/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="https://www.ibudanbalita.com/momenwow/assets/images/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://www.ibudanbalita.com/momenwow/assets/images/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="https://www.ibudanbalita.com/momenwow/assets/images/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://www.ibudanbalita.com/momenwow/assets/images/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="https://www.ibudanbalita.com/momenwow/assets/images/favicons/apple-icon-120x120.png">
    <link rel="icon" type="image/png" href="https://www.ibudanbalita.com/momenwow/assets/images/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="https://www.ibudanbalita.com/momenwow/assets/images/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="https://www.ibudanbalita.com/momenwow/assets/images/favicons/favicon-96x96.png" sizes="96x96">


  <link rel="stylesheet" href="{{ asset('assets/semangatibu/css/bootstrap.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/semangatibu/css/site.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/semangatibu/css/new.css') }}">
  <div id="fb-root"></div>

  <!-- Google Tag Manager -->  
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);  })(window,document,'script','dataLayer','GTM-MR47SS7');</script>  <!-- End Google Tag Manager -->
</head>