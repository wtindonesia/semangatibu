<!DOCTYPE html>
<html lang="en" xmlns:fb="http://ogp.me/ns/fb#" class="gr__ibudanbalita_com mti-inactive">
  @include('layouts._partials.head')
  	@yield('styles')
  <body data-gr-c-s-loaded="true" class="" style="">
    @include('_partials.headers')
    
    <div class="app-container gerak-123-container">
      @yield('content')
    </div>

    @include('_partials.footers')
      @yield('scripts')
  </body>
</html>
