
var assetsDir = 'assets/',
    imagesDir = assetsDir + 'images/',
    scriptsDir = assetsDir + 'js/',
    scssDir = assetsDir + 'scss/',
    stylesDir = assetsDir + 'css/';

var gulp = require('gulp');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
// var pug = require('gulp-pug');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var livereload = require('gulp-livereload');
// var spritesmith = require('gulp.spritesmith');
var gulpWait = require('gulp-wait');

gulp.task('styles', function() {
  return gulp.src([scssDir + '**/*.scss'])
            .pipe(sourcemaps.init())
            .pipe(gulpWait(200))
            .pipe(sass())
            .pipe(rename({suffix: '.prefixed'}))
            .pipe(autoprefixer({
              browsers: ['last 2 versions', 'ie 9']
            }))
            .pipe(gulp.dest(stylesDir))
            .pipe(rename({suffix: '.min'}))
            .pipe(csso({
              report: 'gzip'
            }))
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(stylesDir))
            .pipe(livereload());
});

gulp.task('js', function() {
  return gulp.src([scriptsDir + 'src/main.js'])
              .pipe(sourcemaps.init())
              .pipe(concat('app.js'))
              .pipe(gulp.dest(scriptsDir))
              .pipe(uglify())
              .pipe(rename({suffix: '.min'}))
              .pipe(sourcemaps.write('.'))
              .pipe(gulp.dest(scriptsDir))
              .pipe(livereload())
              .on('error', gutil.log);
});

gulp.task('imagemin', function() {
  return gulp.src(imagesDir + '**/*.*')
              .pipe(imagemin({
                optimizationLevel: 7,
                progressive: true,
                interlaced: true,
                svgoPlugins: [{removeViewBox: false}],
                use: [pngquant()]
              }))
              .pipe(gulp.dest(imagesDir));
});

gulp.task('watch', function() {
  livereload.listen();
  gulp.watch(scssDir + '**/*.scss', ['styles']);
  gulp.watch(scriptsDir + 'src/*.js', ['js']);
});

gulp.task('optimg', ['imagemin']);