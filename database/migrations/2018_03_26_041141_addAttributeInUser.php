<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributeInUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('mom_name');
            $table->string('child_name');
            $table->longText('address');
            $table->string('phone_number');
            $table->date('child_dob');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('mom_name');
            $table->dropColumn('child_name');
            $table->dropColumn('address');
            $table->dropColumn('phone_number');
            $table->dropColumn('child_dob');
        });
    }
}
