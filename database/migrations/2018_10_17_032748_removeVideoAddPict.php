<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveVideoAddPict extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('submission_mother', function (Blueprint $table) {
            $table->dropColumn('video_submission');
            $table->dropColumn('receipt');
            $table->string('picture_submission')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('submission_mother', function (Blueprint $table) {
            $table->string('video_submission')->nullable();
            $table->string('receipt')->nullable();
            $table->dropColumn('picture_submission');
        });
    }
}
