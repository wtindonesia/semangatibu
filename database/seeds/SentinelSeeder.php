<?php

use Illuminate\Database\Seeder;

class SentinelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Sentinel::getRoleRepository()->createModel()->create(
        [
            'name' => 'administrator',
            'slug' => 'admin',
            'permissions'=>[
                'user.admin' => true,
                'user.manager'=>true
            ]
        ]);
        Sentinel::getRoleRepository()->createModel()->create(
        [
            'name' => 'moderator',
            'slug' => 'moderator',
            'permissions'=>[
                'user.admin'=>true,
                'user.update' =>true,
                'user.view' => true
            ]
        ]);
        Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'member',
            'slug' => 'member',
            'permissions'=>[
                'user.update' =>true,
                'user.view' => true
            ]
        ]);


        //grant permission for admin
        
        $role = Sentinel::findRoleBySlug('admin');

        $credentials = [
            'email'    => 'admin@app.com',
            'password' => 'admin123456',
            'first_name'=>'Admin',
            'last_name'=>'',
            'permissions'=> $role->permissions
        ];

        $user = Sentinel::registerAndActivate($credentials);

        //grant permission as admin.manager

        $user->roles()->attach($role);

    }
}
