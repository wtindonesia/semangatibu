<?php

namespace App\Mail;

use App\User;
use Cartalyst\Sentinel\Activations\EloquentActivation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserActivation extends Mailable {

	use Queueable, SerializesModels;

	public $user;
	public $activation;

	/**
	 * Create a new message instance.
	 *
	 * @param User $user
	 * @param EloquentActivation $activation
	 */
	public function __construct(User $user, EloquentActivation $activation)
	{
		$this->user = $user;
		$this->activation = $activation;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this->subject('Aktivasi Email')
			->view('email.user_activation');
	}
}
