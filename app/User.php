<?php

namespace App;
use Cartalyst\Sentinel\Users\EloquentUser as CartalystUser;
use Sentinel;

class User extends CartalystUser {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';

    protected $fillable = [
        'email',
        'password',
        'first_name', 
        'last_name',
        'phone_number',
        'mom_name',
        'child_name',
        'child_dob',
        'address',
        'permissions',
        'facebook_id'
    ];


    public function role()
    {
        return $this->belongsToMany(Role::class, 'role_users', 'user_id', 'role_id');
    }

    public function submission()
    {
        return $this->hasMany('App\SubmissionMother');
    }

}
