<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
       if($user = Sentinel::check()) {
            if ($user->hasAccess('user.admin')) {
                return redirect()->route('admin.home');
            } else {
                return redirect()->route('home');
            }

        }

        return $next($request);
    }
}
