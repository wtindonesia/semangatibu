<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

use App\Http\Requests;

class HomeController extends Controller
{	
	public $pathVideos;
	
	public function __construct()
	{
		$this->pathVideos = public_path('assets/files/momenwow/');
		if(Session::has('is_logged_in') && Session::get('is_logged_in') == true){
            $loggedUser = true;
        }else{
            $loggedUser = false;
        }
        view()->share('loggedUser', $loggedUser);
	}

    public function index()
    {	
    	//forget fb session when login
    	Session::forget('facebook_session');
    	$title = 'Gerak 123';
    	$class_navbar = 'navbar-transparent navbar-padding-top';
    	return view( 'pages.landing.index', compact('title', 'class_navbar') );
    }

    public function getDownloadFile()
    {	
    	$thefile = 'GERAK_123_FINAL.mp4';
    	$path = $this->pathVideos . $thefile;
		return response()->download($path);
    }
}
