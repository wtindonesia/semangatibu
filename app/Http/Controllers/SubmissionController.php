<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\UploadFileRequest;
use App\SubmissionMother;
use File;
use Sentinel;
use Session;
use Storage;
use Image;


class SubmissionController extends Controller
{	
	private $pathPhotos;
	private $pathThumbnail;
	private $credentialOauthAPI;

	public function __construct()
	{
		$this->pathPhotos = public_path('upload/photos/');
		$this->pathThumbnail = public_path('upload/photos/thumbnail/');
		$this->credentialOauthAPI = public_path() . '/php-yt-oauth2.json';

		if(Session::has('is_logged_in') && Session::get('is_logged_in') == true){
            $loggedUser = true;
        }else{
            $loggedUser = false;
        }
        view()->share('loggedUser', $loggedUser);
	}

	public function getUpload()
	{
		//forget fb session when login
    	Session::forget('facebook_session');
		return view('pages.users.fileupload');
	}

	public function postUpload(UploadFileRequest $request)
	{	

        //for receipt
        $picture_submission = $request->file('picture_submission');
        $name = preg_replace('/\..+$/', '', $picture_submission->getClientOriginalName());
        $input['name'] = md5(rand()) . "_" . time() . "." .  $picture_submission->getClientOriginalExtension();

        //check folder
        if (!File::exists($this->pathThumbnail))
		{
			File::makeDirectory($this->pathThumbnail, 0777, true, true);
		}

        //resizing
        $img = Image::make($request->file('picture_submission')->getRealPath());
        $img->orientate();
        $img->fit(300, 300, function ($constraint) {
		    $constraint->aspectRatio();
		})->save($this->pathThumbnail . $input['name']);
        
        //check folder
        if (!File::exists($this->pathPhotos))
		{
			File::makeDirectory($this->pathPhotos, 0777, true, true);
		}

        //real size
        $picture_submission->move($this->pathPhotos, $input['name']);

        //save to db
        $sub = new SubmissionMother();
        $sub->user_id = Sentinel::getUser()->id;
        $sub->picture_submission = $input['name'];
        $sub->picture_description = $request->picture_description;
        $sub->save();

        return redirect()->route('upload.complete');
	}

	public function getUploadComplete()
	{
		return view('pages.users.thankyou');
	}

	public function testgoogle()
	{	
		session_start();
		
		$OAUTH2_CLIENT_ID = '937197534005-70hmqipkajifjdvjl4ru2l1pl43q7q6o.apps.googleusercontent.com';
		$OAUTH2_CLIENT_SECRET = 'RXAENHqCXh4uiB210YHBjtuY';

		define('STDIN',fopen("php://stdin","r"));
		$redirect = 'http://localhost:8080/gerak123/public/upload-submission';
		$json_path = public_path() . '/client_secret.json';
		$credentialOauthAPI = public_path() . '/php-yt-oauth2.json';
		$client = new \Google_Client();
		//$client->setAuthConfigFile(json_decode(file_get_contents($json_path), true));
		$client->setClientId($OAUTH2_CLIENT_ID);
		$client->setClientSecret($OAUTH2_CLIENT_SECRET);
		$client->setScopes('https://www.googleapis.com/auth/youtube');
		$client->setRedirectUri($redirect);
	/*	$client->setAccessType('offline');
		$client->setApplicationName('theName');*/


		$youtube = new \Google_Service_YouTube($client);
		// Check if an auth token exists for the required scopes
		$tokenSessionKey = 'token-' . $client->prepareScopes();
		if (isset($_GET['code'])) {
		  if (strval($_SESSION['state']) !== strval($_GET['state'])) {
		    die('The session state did not match.');
		  }
		  $client->authenticate($_GET['code']);
		  $_SESSION[$tokenSessionKey] = $client->getAccessToken();
		  header('Location: ' . $redirect);
		}

		if (isset($_SESSION[$tokenSessionKey])) {
		  $client->setAccessToken($_SESSION[$tokenSessionKey]);
		}

		// Check to ensure that the access token was successfully acquired.
		if ($client->getAccessToken()) {
		  $htmlBody = '';
		  try{
		    // REPLACE this value with the path to the file you are uploading.
		    $videoPath = "/path/to/file.mp4";
		    // Create a snippet with title, description, tags and category ID
		    // Create an asset resource and set its snippet metadata and type.
		    // This example sets the video's title, description, keyword tags, and
		    // video category.
		    $snippet = new \Google_Service_YouTube_VideoSnippet();
		    $snippet->setTitle("Test title");
		    $snippet->setDescription("Test description");
		    $snippet->setTags(array("tag1", "tag2"));
		    // Numeric video category. See
		    // https://developers.google.com/youtube/v3/docs/videoCategories/list
		    $snippet->setCategoryId("22");
		    // Set the video's status to "public". Valid statuses are "public",
		    // "private" and "unlisted".
		    $status = new \Google_Service_YouTube_VideoStatus();
		    $status->privacyStatus = "public";
		    // Associate the snippet and status objects with a new video resource.
		    $video = new \Google_Service_YouTube_Video();
		    $video->setSnippet($snippet);
		    $video->setStatus($status);
		    // Specify the size of each chunk of data, in bytes. Set a higher value for
		    // reliable connection as fewer chunks lead to faster uploads. Set a lower
		    // value for better recovery on less reliable connections.
		    $chunkSizeBytes = 1 * 1024 * 1024;
		    // Setting the defer flag to true tells the client to return a request which can be called
		    // with ->execute(); instead of making the API call immediately.
		    $client->setDefer(true);
		    // Create a request for the API's videos.insert method to create and upload the video.
		    $insertRequest = $youtube->videos->insert("status,snippet", $video);
		    // Create a MediaFileUpload object for resumable uploads.
		    $media = new \Google_Http_MediaFileUpload(
		        $client,
		        $insertRequest,
		        'video/*',
		        null,
		        true,
		        $chunkSizeBytes
		    );
		    $media->setFileSize(filesize($videoPath));
		    // Read the media file and upload it chunk by chunk.
		    $status = false;
		    $handle = fopen($videoPath, "rb");
		    while (!$status && !feof($handle)) {
		      $chunk = fread($handle, $chunkSizeBytes);
		      $status = $media->nextChunk($chunk);
		    }
		    fclose($handle);
		    // If you want to make other calls after the file upload, set setDefer back to false
		    $client->setDefer(false);
		    
		    $htmlBody .= "<h3>Video Uploaded</h3><ul>";
		    $htmlBody .= sprintf('<li>%s (%s)</li>',
		        $status['snippet']['title'],
		        $status['id']);
		    $htmlBody .= '</ul>';

		  } catch (\Google_Service_Exception $e) {
		    $htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
		        htmlspecialchars($e->getMessage()));
		  } catch (\Google_Exception $e) {
		    $htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
		        htmlspecialchars($e->getMessage()));
		  }
		  $_SESSION[$tokenSessionKey] = $client->getAccessToken();
		} elseif ($OAUTH2_CLIENT_ID == 'REPLACE_ME') {
		  $htmlBody = "<h3>Client Credentials Required</h3>
		  <p>
		    You need to set <code>\$OAUTH2_CLIENT_ID</code> and
		    <code>\$OAUTH2_CLIENT_ID</code> before proceeding.
		  <p>";
		} else {
		  // If the user hasn't authorized the app, initiate the OAuth flow
		  $state = mt_rand();
		  $client->setState($state);
		  $_SESSION['state'] = $state;
		  $authUrl = $client->createAuthUrl();
		  $htmlBody ="<h3>Authorization Required</h3>
		  <p>You need to <a href=" . $authUrl . ">authorize access</a> before proceeding.<p>";
		}

		echo $htmlBody;
	}

	public function testgoogledua()
	{
		session_start();

		/*
		 * You can acquire an OAuth 2.0 client ID and client secret from the
		 * {{ Google Cloud Console }} <{{ https://cloud.google.com/console }}>
		 * For more information about using OAuth 2.0 to access Google APIs, please see:
		 * <https://developers.google.com/youtube/v3/guides/authentication>
		 * Please ensure that you have enabled the YouTube Data API for your project.
		 */
		$OAUTH2_CLIENT_ID = '937197534005-70hmqipkajifjdvjl4ru2l1pl43q7q6o.apps.googleusercontent.com';
		$OAUTH2_CLIENT_SECRET = 'RXAENHqCXh4uiB210YHBjtuY';

		$client = new \Google_Client();
		$client->setClientId($OAUTH2_CLIENT_ID);
		$client->setClientSecret($OAUTH2_CLIENT_SECRET);
		$client->setScopes('https://www.googleapis.com/auth/youtube');
		//$redirect = filter_var('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'], FILTER_SANITIZE_URL);
		$redirect = 'http://localhost:8080/gerak123/public/testgoogledua';
		$client->setRedirectUri($redirect);

		// Define an object that will be used to make all API requests.
		$youtube = new \Google_Service_YouTube($client);

		// Check if an auth token exists for the required scopes
		$tokenSessionKey = 'token-' . $client->prepareScopes();
		if (isset($_GET['code'])) {
		  if (strval($_SESSION['state']) !== strval($_GET['state'])) {
		    die('The session state did not match.');
		  }

		  $client->authenticate($_GET['code']);
		  $_SESSION[$tokenSessionKey] = $client->getAccessToken();
		  echo "Access Token: " . $_SESSION[$tokenSessionKey]['access_token'];
		}

		if (isset($_SESSION[$tokenSessionKey])) {
		  $client->setAccessToken($_SESSION[$tokenSessionKey]);
		}

		// Check to ensure that the access token was successfully acquired.
		if ($client->getAccessToken()) {
		  $htmlBody = '';
		  try{
		    // REPLACE this value with the path to the file you are uploading.
		    $videoPath = "/path/to/file.mp4";

		    // Create a snippet with title, description, tags and category ID
		    // Create an asset resource and set its snippet metadata and type.
		    // This example sets the video's title, description, keyword tags, and
		    // video category.
		    $snippet = new \Google_Service_YouTube_VideoSnippet();
		    $snippet->setTitle("Test title");
		    $snippet->setDescription("Test description");
		    $snippet->setTags(array("tag1", "tag2"));

		    // Numeric video category. See
		    // https://developers.google.com/youtube/v3/docs/videoCategories/list
		    $snippet->setCategoryId("22");

		    // Set the video's status to "public". Valid statuses are "public",
		    // "private" and "unlisted".
		    $status = new \Google_Service_YouTube_VideoStatus();
		    $status->privacyStatus = "public";

		    // Associate the snippet and status objects with a new video resource.
		    $video = new \Google_Service_YouTube_Video();
		    $video->setSnippet($snippet);
		    $video->setStatus($status);

		    // Specify the size of each chunk of data, in bytes. Set a higher value for
		    // reliable connection as fewer chunks lead to faster uploads. Set a lower
		    // value for better recovery on less reliable connections.
		    $chunkSizeBytes = 1 * 1024 * 1024;

		    // Setting the defer flag to true tells the client to return a request which can be called
		    // with ->execute(); instead of making the API call immediately.
		    $client->setDefer(true);

		    // Create a request for the API's videos.insert method to create and upload the video.
		    $insertRequest = $youtube->videos->insert("status,snippet", $video);

		    // Create a MediaFileUpload object for resumable uploads.
		    $media = new \Google_Http_MediaFileUpload(
		        $client,
		        $insertRequest,
		        'video/*',
		        null,
		        true,
		        $chunkSizeBytes
		    );
		    $media->setFileSize(filesize($videoPath));


		    // Read the media file and upload it chunk by chunk.
		    $status = false;
		    $handle = fopen($videoPath, "rb");
		    while (!$status && !feof($handle)) {
		      $chunk = fread($handle, $chunkSizeBytes);
		      $status = $media->nextChunk($chunk);
		    }

		    fclose($handle);

		    // If you want to make other calls after the file upload, set setDefer back to false
		    $client->setDefer(false);


		    $htmlBody .= "<h3>Video Uploaded</h3><ul>";
		    $htmlBody .= sprintf('<li>%s (%s)</li>',
		        $status['snippet']['title'],
		        $status['id']);

		    $htmlBody .= '</ul>';

		  } catch (Google_Service_Exception $e) {
		    $htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
		        htmlspecialchars($e->getMessage()));
		  } catch (Google_Exception $e) {
		    $htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
		        htmlspecialchars($e->getMessage()));
		  }

		  $_SESSION[$tokenSessionKey] = $client->getAccessToken();
		} elseif ($OAUTH2_CLIENT_ID == 'REPLACE_ME') {
		  $htmlBody = "<h3>Client Credentials Required</h3>
		  <p>
		    You need to set <code>\$OAUTH2_CLIENT_ID</code> and
		    <code>\$OAUTH2_CLIENT_ID</code> before proceeding.
		  <p>";
		END;
		} else {
		  // If the user hasn't authorized the app, initiate the OAuth flow
		  $state = mt_rand();
		  $client->setState($state);
		  $_SESSION['state'] = $state;

		  $authUrl = $client->createAuthUrl();
		  $htmlBody = "<h3>Authorization Required</h3>
		  <p>You need to <a href=" . $authUrl . ">authorize access</a> before proceeding.<p>";
		}
		echo $htmlBody;
	}

	public function getDownloadFile($filename)
	{	
		$file = SubmissionMother::where(['id' => $filename])->first();
		$thefile = $file->picture_submission;
		$path = $this->pathPhotos . $thefile;
		return response()->download($path);
	}
}
