<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\MessageBag;
use Input;
use Sentinel;
use Validator;
use View;
use App\User;

class AuthController extends Controller 
{
	public function getIndex()
	{
		return view('admin.layout.login');
	}

	public function postLogin(Requests $request)
    {
    	$rules = array(
			'email'    => 'required',
			'password' => 'required'
		);

		$input = $request->all();
		$validation = Validator::make($input, $rules);
		if (!$validation->fails()) {
	        try {
	            $user = Sentinel::authenticate($input);
	            if($user){
					return redirect()->route('admin.home');
				}
	        } catch (WrongCredentialException $e) {
	            $error = with(new MessageBag())->add('error', $e->getMessage());
	            return redirect()->back()->withInput()->withErrors($error);
	        }
    	}else{
    		$error = with(new MessageBag())->add('error', $e->getMessage());
    		return redirect()->back()->withInput()->withErrors($error);
    	}
    }

	public function logout()
	{
		Sentinel::logout();

		return redirect()->route('admin.login');
	}
}