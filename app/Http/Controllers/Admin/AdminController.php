<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use View;
use App\User;
use App\SubmissionMother;


class AdminController extends Controller
{ 
	public function index()
	{
		return view('admin.pages.home');
	}

	public function getDatauser(Request $request)
	{
		$limit = 15;
		$keyword = $request->keyword;
		$users = User::whereRaw("email <> 'admin@app.com' 
								and (
									mom_name like '%" . $keyword . "%' 
									or email like '%" . $keyword . "%' 
								) ")
				->paginate($limit);
		return view('admin.pages.user', compact('users', 'keyword'));
	}

	public function getDatasubmission(Request $request)
	{
		$limit = 15;
		$keyword = $request->keyword;
		$submission = SubmissionMother::whereHas('user', function($query) use ($keyword){
			$query->where('mom_name', 'like', '%' . $keyword . '%')
			->orWhere('email', 'like', '%' . $keyword . '%');
		})->paginate($limit);
		return view('admin.pages.submission', compact('submission', 'keyword'));
		
	}

	public function changeVisible(Request $request)
	{
		$values = $request->get('values');
		$id = $request->get('id');

		$data = SubmissionMother::where('id', '=', $id)->first();
		$update = true;
		$update_values = 1;
		if($values == true){
			$update = false;
			$update_values = 0;
		}

		$data->is_visible = $update;

		$ret = 'fail';
		if($data->save()) {
			$ret = 'ok';
		}
		$vis = 'not displayed';
		if($data->is_visible == true){
			$vis = 'displayed';
		}

		return compact('ret', 'vis', 'update_values');

	}
}