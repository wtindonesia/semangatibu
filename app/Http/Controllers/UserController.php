<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserGateway;
use App\Http\Requests\RegisterUserRequest;
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookAuthorizationException;
use Facebook\FacebookRequestException;
use Facebook\GraphObject;
use Facebook\GraphUser;
use App\Exceptions\User\AlreadyActivateException;
use App\Exceptions\User\UserNotFoundException;
use App\Exceptions\User\WrongCredentialException;
use App\Exceptions\User\NotActivatedException;
use App\User;
use App\SubmissionMother;
use Sentinel;
use Session;


class UserController extends Controller
{	
	protected $userGateway;
	public $limit;

	public function __construct(UserGateway $userGateway)
	{
		$this->userGateway = $userGateway;
		$this->limit = 4;
        
        if(Session::has('is_logged_in') && Session::get('is_logged_in') == true){
            $loggedUser = true;
        }else{
            $loggedUser = false;
        }
        view()->share('loggedUser', $loggedUser);
	}
  public function getRegister()
	{	
		$input = $this->getInput();
		$is_facebook_session = Session::has('facebook_session');
		return view('pages.users.register', compact('input', 'is_facebook_session'));
	}

	public function postRegister(RegisterUserRequest $request)
	{	
		if(Session::has('facebook_session')){
			$user = $this->userGateway->createUserFb($request);
			$login = $this->userGateway->loginById($user->id);
			//create session for logged in user
			if($login){
				Session::put('is_logged_in', true);
			}
			return redirect()->route('upload.submission');
		}else{
			$user = $this->userGateway->createUser($request);
		}

		try {
			//$this->userGateway->sendActivationEmail($user);
			$auto = $this->userGateway->autoActivation($user);
			$login = $this->userGateway->loginById($user->id);
			if($login){
				Session::put('is_logged_in', true);
			}
			
			return redirect()->route('upload.submission');
			//return redirect()->route('register.complete');

		} catch (AlreadyActivateException $e) {
			echo "User already activated";
			return redirect()->route('upload.submission');
			//return redirect()->route('home');
		}
	}

	private function getInput()
	{
		$input = [
			'facebook_id'      	=> null,
			'nama_ibu'  		=> '',
			'nama_anak'        => '',
			'tanggal_lahir_anak'       	=> '',
			'email_ibu'             => '',
			'nomor_handphone'    	=> ''
		];

		if(Session::has('facebook_session')){
			$input['facebook_id'] = Session::get('facebook_session')['facebook_id'];
			$input['nama_ibu'] = Session::get('facebook_session')['name'];
			$input['email_ibu'] = Session::get('facebook_session')['email'];
		}

		if (old()) {
			$input = array_merge($input, old());
		}

		return $input;
	}

	public function activateUser($user_id, $code)
	{
		try{
			$this->userGateway->checkActivationCode($user_id, $code);
		}catch(UserNotFoundException $e){
			Log::error('activateAndUpdatePassword: UserNotFoundException');

			return redirect()->route('home');
		}catch(AlreadyActivateException $e){
			return redirect()->route('login');
		}

		$this->userGateway->activateUser($user_id, $code);
		$this->userGateway->loginById($user_id);
		return redirect()->route('upload.submission');
	}

	public function registerComplete()
	{
		return view('pages.users.register-complete');
	}

	public function facebookLogin(Request $request)
    {          
        session_start();     
        FacebookSession::setDefaultApplication(config('services.facebook.client_id'),config('services.facebook.client_secret'));
        $redirect_url = route('user.fblogin');
        $helper = new FacebookRedirectLoginHelper($redirect_url);
        $fbloginurl = $helper->getLoginUrl(array('scope' => 'public_profile,email'));
        $state = md5(rand());
        $request->session()->set('g_state', $state);
        return redirect()->to($fbloginurl);
    }

    public function fbSignUp(Request $request)
    {
    	session_start(); 
        FacebookSession::setDefaultApplication(config('services.facebook.client_id'),config('services.facebook.client_secret'));        
        $redirect_url = route('user.fblogin');
        $helper = new FacebookRedirectLoginHelper(
            $redirect_url,
            config('services.facebook.client_id'),
            config('services.facebook.client_secret')
        ); 
        try
        {
            $session = $helper->getSessionFromRedirect();       
        } catch (FacebookRequestException $ex){
            return $ex->getMessage();           
        } catch (\Exception $ex){
            return $ex->getMessage();
        }
        if (isset($session) && $session){           
            try
            {
                $user_profile = (new FacebookRequest(
                    $session, 'GET', '/me?fields=id,name,first_name,last_name,email,photos,address'
                ))->execute()->getGraphObject(GraphUser::className());
       			
       			//set session for fb
       			$session_fb = array();
       			$session_fb['facebook_id'] = $user_profile->getProperty('id');
       			$session_fb['email'] = $user_profile->getProperty('email');
       			$session_fb['name'] = $user_profile->getProperty('name');
       			$session_fb['first_name'] = $user_profile->getProperty('first_name');
       			$session_fb['last_name'] = $user_profile->getProperty('last_name');

                Session::put('facebook_session', $session_fb);

                //exist condition
                $is_fb_exists = false;
                if(User::where('email',$user_profile->getProperty('email'))->first()){
                	$is_fb_exists = true;
                }else{
                	if(User::where('facebook_id',$user_profile->getProperty('id'))->first()){
	                	$is_fb_exists = true;
	                }
                }

                if ($is_fb_exists == true) {

                    $user = $this->userGateway->getUserByFacebookID($user_profile->getProperty('id'), $user_profile->getProperty('email'));
                    if (!$this->userGateway->userIsActivated($user)) {
						session(['resend_email_user_id' => $user->id]);
						return redirect()->route('register');
					}
					Session::put('is_logged_in', true);
                    $this->userGateway->loginById($user->id);
                } else { 
					return redirect()->route('register');
                }               
            } catch (FacebookRequestException $e) {
                echo "Exception occured, code: " . $e->getCode();
                echo " with message: " . $e->getMessage();
            }
        }
        return redirect()->route('upload.submission');
   }

   	public function getNotActivated()
	{
		if (!session()->has('resend_email_user_id')) {
			return redirect()->route('home');
		}
		$class_navbar = 'navbar-transparent navbar-padding-top';
		return view('pages.users.not-activated', compact('class_navbar'));
	}

	public function getRequestResendActivationEmail()
	{
		session()->forget('resend_email_user_id');

		return view('pages.users.request-send-activation-email');
	}

	public function postRequestResendActivationEmail(Request $request)
	{
		$user = $this->userGateway->emailExists($request->email);
		if (!$user) {
			return redirect()->back()->withErrors('Email tidak terdaftar');
		}
		session(['resend_email_user_id' => $user->id]);

		return redirect()->route('user.resend.activation.email.submit');
	}

	public function resendActivationEmail()
	{
		$user_id = session('resend_email_user_id');
		if (!$user_id) {
			Log::error('resendActivationEmail: don\'t have session resend_email_user_id');

			return redirect()->route('login');
		}
		$can_resend = true;
		$this->userGateway->resendActivationEmail($user_id);
		$message = 'Email telah terkirim. Harap mengecek email untuk melakukan aktivasi.';

		return redirect()->route('user.resend.activation.email')->with(['message' => $message, 'can_resend' => $can_resend]);
	}

	public function getResendActivationEmail()
	{
		$message = session('message', 'Harap mengecek email Mum untuk melakukan aktivasi.');
		$can_resend = session('can_resend', true);
		$class_navbar = 'navbar-transparent navbar-padding-top';
		return view('pages.users.resend-email-activation', compact('message', 'can_resend', 'class_navbar'));
	}

	public function getGallery(Request $request)
	{	
		$data = SubmissionMother::orderBy('created_at', 'DESC')->where('is_visible', '=', true)->limit($this->limit)->get();
		$gallery = array();
		foreach($data as $val){
			$gallery[] = array(
				'id' => $val->id,
				'mom_name' => $val->user->mom_name,
				'child_name' => $val->user->child_name, 
				'picture_submission' => $val->picture_submission, 
				'years' => $this->dateDiff($val->user->child_dob)
			);
		}
		return view('pages.users.gallery', compact('gallery'));
	}

	public function loadMoreGallery(Request $request)
	{
		$id = $request->lastid;
		$datafrom = $request->from;
		if($datafrom == 'all'){
			$data = SubmissionMother::where('id', '<', $id)->orderBy('created_at','DESC')->where('is_visible', '=', true)->limit($this->limit)->get();
			$from = 'all';
		}else{
			$data = SubmissionMother::where('id', '<', $id)->orderBy('created_at','DESC')->where('is_visible', '=', true)->where('user_id', '=', Sentinel::getUser()->id)->limit($this->limit)->get();
			$from = 'onlyme';
		}
		if(count($data) > 0){
			foreach($data as $val){
				$gallery[] = array(
					'id' => $val->id,
					'mom_name' => $val->user->mom_name,
					'child_name' => $val->user->child_name, 
					'picture_submission' => $val->picture_submission, 
					'years' => $this->dateDiff($val->user->child_dob)
				);
			}
			return (String) view('pages.users.single-gallery', compact('gallery', 'from'));
		}else{
			return '0';
		}
	}

	public function getGalleryOnlyMe(Request $request)
	{	
		$data = SubmissionMother::orderBy('created_at', 'DESC')->where('user_id', '=', Sentinel::getUser()->id)->where('is_visible', '=', true)->limit($this->limit)->get();
		$gallery = array();
		if(count($data) > 0){
			foreach($data as $val){
				$gallery[] = array(
					'id' => $val->id,
					'mom_name' => $val->user->mom_name,
					'child_name' => $val->user->child_name, 
					'picture_submission' => $val->picture_submission, 
					'years' => $this->dateDiff($val->user->child_dob)
				);
			}
			$from = 'onlyme';
			return (String) view('pages.users.single-gallery', compact('gallery', 'from'));
		}else{
			return '0';
		}
	}

	public function getGalleryAll(Request $request)
	{	
		$data = SubmissionMother::orderBy('created_at', 'DESC')->where('is_visible', '=', true)->limit($this->limit)->get();
		$gallery = array();
		if(count($data) > 0){
			foreach($data as $val){
				$gallery[] = array(
					'id' => $val->id,
					'mom_name' => $val->user->mom_name,
					'child_name' => $val->user->child_name, 
					'picture_submission' => $val->picture_submission, 
					'years' => $this->dateDiff($val->user->child_dob)
				);
			}
			$from = 'all';
			return (String) view('pages.users.single-gallery', compact('gallery', 'from'));
		}else{
			return '0';
		}
	}

	public function getGalleryDetail(Request $request, $id_submission)
	{	

		$data = SubmissionMother::where('id', '=', $id_submission)->where('is_visible', '=', true)->first();
		if(count($data) > 0){
			$gallery = array(
				'id' => $data->id,
				'mom_name' => $data->user->mom_name,
				'child_name' => $data->user->child_name, 
				'picture_submission' => $data->picture_submission, 
				'picture_description' => $data->picture_description, 
				'years' => $this->dateDiff($data->user->child_dob)
			);


			$agent = $request->header('User-Agent');
			$stringwa = array('Dengan Primanutri, Dukung #SemangatIbu ' .  request()->fullUrl());
			$washare = $stringwa[0];
			if( strpos($agent, "iPhone") || strpos($agent, "Android")){
				$wa = http_build_query($stringwa);
				$washare = substr($wa, 2);
			}
			
			return view('pages.users.gallery-detail', compact('gallery', 'washare'));
		}
		return abort(404);
	}

	public function dateDiff($child_date)
	{	
		$now = date('Y-m-d');
		$d1 = new \DateTime($now);
		$d2 = new \DateTime($child_date);

		$diff = $d2->diff($d1);

		return $diff->y;
	}

}
