<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Exceptions\User\WrongCredentialException;
use App\Exceptions\User\NotActivatedException;
use App\Services\UserGateway;
use Illuminate\Support\MessageBag;
use Sentinel;
use Session;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    //use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    //protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    /*public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }*/

    protected $userGateway;

    public function __construct(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
        
        if(Session::has('is_logged_in') && Session::get('is_logged_in') == true){
            $loggedUser = true;
        }else{
            $loggedUser = false;
        }
        view()->share('loggedUser', $loggedUser);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function logout()
    {   
        $this->userGateway->logout();
        Session::forget('is_logged_in');
        return redirect()->route('home');
    }

    public function getLogin()
    {   
        $class_navbar = 'navbar-transparent navbar-padding-top';
        return view('pages.users.login', compact('class_navbar'));
    }

    public function postLogin(Requests\LoginRequest $request)
    {
        try {
            $this->userGateway->login($request->email, $request->password);
            $user = Sentinel::check();
            if ($user->hasAccess('user.admin')) {
                return redirect()->route('admin.home');
            } else {
                Session::put('is_logged_in', true);
                return redirect()->route('upload.submission');
            }
        } catch (WrongCredentialException $e) {
            $error = with(new MessageBag())->add('error', $e->getMessage());

            //return redirect()->back()->withInput()->withErrors($error);
            return redirect('admin-manager/login')->with('errors', $error);
        } catch (NotActivatedException $e) {
            $user = $this->userGateway->getCredentialByEmail($request->get('email'));
            session(['resend_email_user_id' => $user->id]);

            return redirect()->route('user.not.activated');
        } 
    }

    public function postLoginAjax(Requests\LoginRequest $request)
    {   
        try {
            $this->userGateway->login($request->email, $request->password);
            $user = Sentinel::check();
            Session::put('is_logged_in', true);
            $return['redirect'] = route('upload.submission');
            $return['message'] = 'success';
            return json_encode($return);
        }catch(WrongCredentialException $e){
            $return['error'] = $e->getMessage();
            $return['message'] = 'fail';
            return json_encode($return);
        }



    }
}
