<?php

namespace App\Http\Requests;


class RegisterUserRequest extends Request
{
    private $rules = [
			'nama_ibu'         => 'required|regex:/^([A-Za-z ])+$/',
			'nama_anak'       => 'required|regex:/^([A-Za-z ])+$/|min:3',
			'tanggal_lahir_anak' => 'required|date',
			'email_ibu'        => 'required|email|unique:users,email',
			'nomor_handphone'  => 'required|numeric|regex:/^0[0-9]{8,12}$/|unique:users,phone_number',
			'password' 		   => 'required|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^A-Za-z0-9]).{8,}$/',
			'password.regex'   => 'Password minimal 8 karakter dan terdiri dari 1 huruf kapital, 1 angka, dan 1 simbol spesial.'
		];

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return $this->rules;
	}
}
