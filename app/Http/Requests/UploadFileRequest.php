<?php

namespace App\Http\Requests;


class UploadFileRequest extends Request
{
	private $rules = [
		'picture_submission' => 'required|mimes:jpeg,png,jpg,gif,svg|max:5000',
		'picture_description' => 'required|max:400',
	];


	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return $this->rules;
	}

}