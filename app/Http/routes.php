<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => '/', 'uses' => 'HomeController@index']);
Route::get('/#caraikutan', ['as' => '/#caraikutan', 'uses' => 'HomeController@index']);
Route::get('/#downloadvideolagu', ['as' => '/#downloadvideolagu', 'uses' => 'HomeController@index']);
// Route::get('/#downloadvideo', ['as' => '/#downloadvideo', 'uses' => 'HomeController@index']);
// Route::get('/#downloadlagu', ['as' => '/#downloadlagu', 'uses' => 'HomeController@index']);
Route::get('/downloadvideo123', ['as' => 'downloadvideo123', 'uses' => 'HomeController@getDownloadFile']);

Route::get('home', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('testgoogle', ['as' => 'testgoogle', 'uses' => 'SubmissionController@testgoogle']);
Route::get('testgoogledua', ['as' => 'testgoogledua', 'uses' => 'SubmissionController@testgoogledua']);


Route::group(['middleware' => 'guest'], function () {
	Route::get('/login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
	Route::post('/login', ['as' => 'login.submit', 'uses' => 'Auth\AuthController@postLogin']);
	Route::post('/loginajax', ['as' => 'login.submit.ajax', 'uses' => 'Auth\AuthController@postLoginAjax']);

	Route::get('/register', ['as' => 'register', 'uses' => 'UserController@getRegister']);
	Route::post('/register', ['as' => 'register.submit', 'uses' => 'UserController@postRegister']);
	Route::get('/activate/{id}/{code}', ['as' => 'user.activate', 'uses' => 'UserController@activateUser']);
	Route::get('/register-complete', ['as' => 'register.complete', 'uses' => 'UserController@registerComplete']);

	Route::get('user/login/callback',array('as'=>'user.fblogin','uses'=>'UserController@fbSignUp')) ;
	Route::get('user/facebook/login',array('as'=>'user.facebook.login','uses'=>'UserController@facebookLogin'));
	Route::get('/user-not-activated',array('as'=>'user.not.activated','uses'=>'UserController@getNotactivated'));
	Route::get('/resend_code', ['as' => 'user.resend.activation.email.submit', 'uses' => 'UserController@resendActivationEmail']);
	Route::post('/request_resend_email', ['as' => 'user.request.resend.activation.email.submit', 'uses' => 'UserController@postRequestResendActivationEmail']);
	Route::get('/resend_code', ['as' => 'user.resend.activation.email.submit', 'uses' => 'UserController@resendActivationEmail']);
	Route::get('/resend_email', ['as' => 'user.resend.activation.email', 'uses' => 'UserController@getResendActivationEmail']);

});
Route::get('gallery', ['as' => 'user.gallery', 'uses' => 'UserController@getGallery']);
Route::get('gallery-loadmore', ['as' => 'user.gallery.loadmore', 'uses' => 'UserController@loadMoreGallery']);
Route::get('gallery-detail/{id_submission}', ['as' => 'user.gallery.detail', 'uses' => 'UserController@getGalleryDetail']);
Route::get('gallery-only-me', ['as' => 'user.gallery.onlyme', 'uses' => 'UserController@getGalleryOnlyMe']);
Route::get('gallery-all', ['as' => 'user.gallery.onlyme', 'uses' => 'UserController@getGalleryAll']);

//to delete this
//Route::get('/register-complete', ['as' => 'register.complete', 'uses' => 'UserController@registerComplete']);

// Route::get('/upload-submission', ['as' => 'upload.submission', 'uses' => 'SubmissionController@getUpload']);
// Route::post('/upload-submission', ['as' => 'upload.submission.submit', 'uses' => 'SubmissionController@postUpload']);
// Route::get('/upload-complete', ['as' => 'upload.complete', 'uses' => 'SubmissionController@getUploadComplete']);

Route::group(['middleware' => 'auth'], function () {
	Route::get('/logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@logout']);
	Route::get('/profile/edit', ['as' => 'profile.edit', 'uses' => 'UserController@editProfile']);
	Route::post('/profile/edit', ['as' => 'profile.update', 'uses' => 'UserController@updateProfile']);

	Route::get('/upload-submission', ['as' => 'upload.submission', 'uses' => 'SubmissionController@getUpload']);
	Route::post('/upload-submission', ['as' => 'upload.submission.submit', 'uses' => 'SubmissionController@postUpload']);
	Route::get('/upload-complete', ['as' => 'upload.complete', 'uses' => 'SubmissionController@getUploadComplete']);

	//change password
	Route::get('/profile/change-password', ['as' => 'password.edit', 'uses' => 'UserController@editPassword']);
	Route::post('/profile/change-password', ['as' => 'password.update', 'uses' => 'UserController@updatePassword']);
	Route::get('/download-file/{filename}', ['as' => 'download.file', 'uses' => 'SubmissionController@getDownloadFile']);
});
