<?php

Route::group(['middleware' => 'auth.admin'], function () {
	Route::get('/', ['as' => 'admin.home', 'uses' => 'AdminController@index']);
	Route::get('/home', ['as' => 'admin.home.home', 'uses' => 'AdminController@index']);
	Route::get('logout', ['as' => 'admin.logout', 'uses' => 'Auth\AuthController@logout']);
	Route::get('data-user', ['as' => 'admin.datauser', 'uses' => 'AdminController@getDatauser']);
	Route::get('data-submission', ['as' => 'admin.datasubmission', 'uses' => 'AdminController@getDatasubmission']);
	Route::get('change-is-visible', ['as' => 'admin.changevisible', 'uses' => 'AdminController@changeVisible']);
});

Route::group(['middleware' => 'guest', 'namespace' => 'Auth'], function(){
	Route::get('login', ['as' => 'admin.login', 'uses' => 'AuthController@getIndex']);
	Route::post('admin.login', ['as' => 'admin.login.post', 'uses' => 'AuthController@postLogin']);
});


