<?php 

namespace App\Services;

use App\SubmissionMother;
use Activation;
use Mail;
use Reminder;
use Sentinel;
use Validator;
use Carbon\Carbon;
use App\Http\Requests\RegisterUserRequest;
use Cartalyst\Sentinel\Users\UserInterface;
use App\Exceptions\User\AlreadyActivateException;
use App\Exceptions\User\UserNotFoundException;
use App\Exceptions\User\WrongCredentialException;
use App\Exceptions\User\NotActivatedException;
use App\User;
use Session;



class UserGateway
{

	/**
	 * @param RegisterUserRequest $request
	 * @param $marketing_source
	 * @return bool|UserInterface
	 */
	public function createUser(RegisterUserRequest $request)
	{
		$role = Sentinel::findRoleBySlug('member');
		$credentials = [
			'email'             => $request->get('email_ibu'),
			'password'          => $request->get('password'),
			'first_name'        => $request->get('nama_ibu'),
			'last_name'         => $request->get('nama_ibu'),
			'mom_name'			=> $request->get('nama_ibu'),
			'child_name'		=> $request->get('nama_anak'),
			'phone_number'		=> $request->get('nomor_handphone'),
			'child_dob'			=> date('Y-m-d', strtotime($request->get('tanggal_lahir_anak'))),
			'permissions'		=> $role->permissions,
		];	
		$user = Sentinel::register($credentials);
		$user->roles()->attach($role);
		return $user;
	}

	public function createUserFb(RegisterUserRequest $request)
	{
		$role = Sentinel::findRoleBySlug('member');
		$credentials = [
			'email'             => $request->get('email_ibu'),
			'password'          => $request->get('password'),
			'first_name'        => $request->get('nama_ibu'),
			'last_name'         => $request->get('nama_ibu'),
			'mom_name'			=> $request->get('nama_ibu'),
			'child_name'		=> $request->get('nama_anak'),
			'phone_number'		=> $request->get('nomor_handphone'),
			'child_dob'			=> date('Y-m-d', strtotime($request->get('tanggal_lahir_anak'))),
			'permissions'		=> $role->permissions,
			'facebook_id'		=> $request->get('facebook_id'),
		];	
		$user = Sentinel::register($credentials);
		$user->roles()->attach($role);
		$activation = $this->autoActivationForFB($user);
		return $user;
	}

	/**
	 * @param UserInterface $user
	 * @throws AlreadyActivateException
	 * @throws EmailNotSentException
	 */
	public function sendActivationEmail(UserInterface $user)
	{
		if(Activation::completed($user)) {
			throw new AlreadyActivateException("User dengan email {$user->getUserLogin()} sudah melakukan aktivasi.", 403);
		}
		$activation = Activation::exists($user);
		if (!$activation) {
			$activation = Activation::create($user);
		}
		
		$data['user'] = $user;
		$data['activation'] = $activation;
		 Mail::send('mail.activation', ['data' => $data], function ($m) use ($user) {
            $m->from('hello@app.com', 'Semangat Ibu Pintar');

            $m->to($user->email, $user->name)->subject('Activation!');
        });

		//$this->saveSuccessSentEmailToLog($user->id, UserEmailSent::TYPE_ACTIVATION);
	}

	public function autoActivation(UserInterface $user)
	{
		if(Activation::completed($user)) {
			throw new AlreadyActivateException("User dengan email {$user->getUserLogin()} sudah melakukan aktivasi.", 403);
		}
		$activation = Activation::exists($user);
		if (!$activation) {
			$activation = Activation::create($user);
		}
		$this->activateUser($user->id, $activation->code);
		return $user;
	}

	public function autoActivationForFB(UserInterface $user)
	{
		if(Activation::completed($user)) {
			throw new AlreadyActivateException("User dengan email {$user->getUserLogin()} sudah melakukan aktivasi.", 403);
		}
		$activation = Activation::exists($user);
		if (!$activation) {
			$activation = Activation::create($user);
		}
		$this->activateUser($user->id, $activation->code);
		return $user;
	}


	/**
	 * @param $user_id
	 * @param $code
	 * @throws AlreadyActivateException
	 * @throws Exception
	 * @throws UserNotFoundException
	 * @throws WrongActivationCodeException
	 */
	public function checkActivationCode($user_id, $code)
	{
		$user = Sentinel::findById($user_id);
		if (!$user) {
			throw new UserNotFoundException('User not found');
		}

		if ($this->userIsActivated($user)) {
			throw new AlreadyActivateException("User {$user_id} already activate", 403);
		}

		$activation = Activation::exists($user);
		if (!$activation) {
			$this->sendActivationEmail($user);
			$message = "Kode Aktivasi telah expired, silahkan mengecek email anda untuk kode aktivasi yang baru.";
			throw new ActivationCodeExpiredException($message);
		} else {
			if ($activation->code != $code) {
				throw new WrongActivationCodeException('Wrong activation code.');
			}
		}
	}

	/**
	 * @param UserInterface $user
	 * @return bool
	 */
	public function userIsActivated(UserInterface $user)
	{
		return Activation::completed($user);
	}

	/**
	 * @param $user_id
	 * @param $code
	 * @return bool
	 */
	public function activateUser($user_id, $code)
	{
		$user = Sentinel::findById($user_id);
		Activation::complete($user, $code);
	}


	public function loginById($user_id)
	{
		if (Sentinel::login(Sentinel::findById($user_id))){
			return true;
		} else {
			return false;
		}

	}

	public function logout()
	{
		Sentinel::logout();
	}

	 /**
	 * @param $email
	 * @param $password
	 * @throws WrongCredentialException
	 * @throws NotActivatedException
	 */
	public function login($email, $password)
	{
		$user = Sentinel::findByCredentials(['email' => $email]);
		if (!$user) {
			throw new WrongCredentialException('Username atau Password salah');
		}

		if (!$this->userIsActivated($user)) {
			throw new NotActivatedException('User belum verifikasi email.');
		}

		$credentials = [
			'email'    => $email,
			'password' => $password
		];
		if (Sentinel::authenticate($credentials)) {

		} else {
			throw new WrongCredentialException('Username atau Password salah');
		}
	}

	
	/**
	 * @param $email
	 * @return UserInterface
	 */
	public function getCredentialByEmail($email)
	{
		$credentials = [
			'email' => $email
		];

		return Sentinel::findByCredentials($credentials);
	}

	/**
	 * @param $facebook_id
	 * @return UserInterface
	 * @throws UserNotFoundException
	 */
	public function getUserByFacebookID($facebook_id, $email)
	{	
		$user = User::where('facebook_id', $facebook_id)->first();
		if (!$user) {
			//throw new UserNotFoundException('Facebook user belum terdaftar.');
			$user = User::where('email', $email)->first();
			$user->facebook_id = $facebook_id;
			$user->save();
		}

		return $user;
	}

	/**
	 * @param $user_id
	 * @throws AlreadyActivateException
	 * @throws ThrottleEmailSendException
	 * @throws UserNotLoginException
	 */
	public function resendActivationEmail($user_id)
	{
		$user = Sentinel::findById($user_id);
		if (!$user) {
			throw new UserNotLoginException('User not login');
		}
		$this->sendActivationEmail($user);
	}
}