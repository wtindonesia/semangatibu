<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubmissionMother extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'submission_mother';

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
