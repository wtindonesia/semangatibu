<?php

namespace App\Providers;
use Session;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {   
        if(Session::has('is_logged_in') && Session::get('is_logged_in') == true){
            $loggedUser = true;
        }else{
            $loggedUser = false;
        }
        view()->share('loggedUser', $loggedUser);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
